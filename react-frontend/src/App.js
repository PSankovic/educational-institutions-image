import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Home from './components/home/Home';
import UsersPage from './components/users/UsersPage';
import Login from './components/authentication/Login';
import * as Constants from './Constants';
import ResultsPage from './components/results/ResultsPage';
import QueryPage from './components/query/QueryPage';
import ActivateAccount from './components/authentication/ActivateAccount';
import EditSectorsPage from './components/edit/EditSectorsPage';
import ThemesProvider from './themes/ThemesProvider';
import EditQueryPage from './components/edit/EditQueryPage';

export const GroupContext = React.createContext([]);

export default function App() {

  const [sectors, setSectors] = React.useState([]);
  const [currentUser, setCurrentUser] = React.useState(null);
  const [dataLoaded, setDataLoaded] = React.useState(false);

  React.useEffect(() => {
    getCurrentUser();
    getSectors();
  }, []);

  function onLogout() {
    fetch(Constants.LOGOUT, {credentials: 'include'})
      .then(response => {
        if (response.ok) {
          setCurrentUser(null);
        } else {
          console.error("Invalid logout operation result.");
        }
      });
  }

  function getCurrentUser() {
    fetch(Constants.GET_CURRENT_USER, {credentials: 'include'})
      .then(response => {
        if (response.ok)
          return response.json()
      })
      .then(data => {
        setCurrentUser(data);
        console.log(data);
      }).catch(() => setCurrentUser(null));
  }

  function getSectors() {
    const startTime = new Date();
    fetch(Constants.GET_ACTIVE_SECTORS)
      .then(response => response.json())
      .then(data => setSectors(data))
      .then(() => {
        if ((new Date() - startTime) / 1000 > 30) {
          setTimeout(getSectors, 5000);
        } else {
          setDataLoaded(true);
        }
      })
  }

  function onLogin() {
    getCurrentUser();
  }

  function generateLink(name) {
    return name.toLowerCase()
      .replace(/ /g, "-")
      .replace(/č/g, "c")
      .replace(/ć/g, "c")
      .replace(/š/g, "s")
      .replace(/đ/g, "d")
      .replace(/ž/g, "z");
  }

  return (
    <GroupContext.Provider value={{currentUser, sectors, getSectors, generateLink, onLogout}}>
      <ThemesProvider>
        <Paper>
          <BrowserRouter>

            <Switch>
              <Route path="/" exact render={() => <Home loaded={dataLoaded}/>}/>
              <Route path="/login" exact render={() => <Login onLogin={onLogin}/>}/>
              <Route path="/activate-account" exact render={() => <ActivateAccount onLogin={onLogin}/>}/>
              {currentUser && currentUser.roles.includes("ADMIN") &&
              <Route path="/users" exact render={() => <UsersPage/>}/>}
              {currentUser && sectors && sectors.map((sector) =>
                <Route key={sector.id} path={`/results/${generateLink(sector.name)}`}
                       exact render={() => <ResultsPage sector={sector}/>}/>)
              }
              {currentUser && <Route path="/edit" exact render={() => <EditSectorsPage/>}/>}
              {sectors && sectors.map((sector) =>
                sector.categories && sector.categories.map((category) =>
                  <Route key={category.id}
                         path={`/questions/${generateLink(sector.name)}/${generateLink(category.name)}`}
                         exact render={() => <QueryPage sectorName={sector.name} sectorDescription={sector.description}
                                                        category={category}/>}/>)
              )}
              {currentUser && sectors && sectors.map((sector) =>
                sector.categories && sector.categories.map((category) =>
                  <Route key={category.id}
                         path={`/edit-questions/${generateLink(sector.name)}/${generateLink(category.name)}`}
                         exact render={() => <EditQueryPage sectorName={sector.name} category={category}/>}/>)
              )}
              {/*<Route component={NoMatchPage}/>*/}
            </Switch>

          </BrowserRouter>

        </Paper>
      </ThemesProvider>
    </GroupContext.Provider>
  );
}
