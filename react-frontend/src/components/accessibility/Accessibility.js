import React from 'react';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InvertColorsIcon from '@material-ui/icons/InvertColors';
import InvertColorsOffIcon from '@material-ui/icons/InvertColorsOff';
import IconButton from '@material-ui/core/IconButton';
import CancelIcon from '@material-ui/icons/Cancel';
import UndoIcon from '@material-ui/icons/Undo';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import {highContrastTheme} from '../../themes/HighContrastTheme';
import {defaultTheme} from '../../themes/DefaultTheme';
import {ThemeContext} from '../../themes/ThemesProvider';

const useStyles = makeStyles(theme => ({
  iconStyle: {
    padding: 0
  },
  iconColor: {
    color: theme.palette.text.textDefault
  },
  border: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  list: {
    width: 240
  }
}));

export default function Accessibility() {

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const setTheme = React.useContext(ThemeContext).setTheme;
  const increaseFontSize = React.useContext(ThemeContext).increaseFontSize;
  const decreaseFontSize = React.useContext(ThemeContext).decreaseFontSize;
  const resetFontSize = React.useContext(ThemeContext).resetFontSize;

  const handleDrawer = () => setOpen(!open);
  const handleDrawerClose = () => setOpen(false);
  const handleKeyDown = (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    handleDrawerClose();
  };

  const list = () => (
    <div role="presentation" onKeyDown={handleKeyDown} className={clsx(classes.list)}>
      <List>
        <ListItem>
          <Button variant="contained" fullWidth color="secondary" className={classes.border}
                  startIcon={<CancelIcon titleAccess="Ikona zatvaranja"/>} onClick={handleDrawerClose}>
            Zatvori
          </Button>
        </ListItem>

        <ListItem button onClick={increaseFontSize}>
          <ListItemIcon><ZoomInIcon className={classes.iconColor}
                                    titleAccess="Ikona povećanja slova"/></ListItemIcon>
          <ListItemText>Povećaj veličinu slova</ListItemText>
        </ListItem>

        <ListItem button onClick={decreaseFontSize}>
          <ListItemIcon><ZoomOutIcon className={classes.iconColor}
                                     titleAccess="Ikona smanjenja slova"/></ListItemIcon>
          <ListItemText>Smanji veličinu slova</ListItemText>
        </ListItem>

        <ListItem button onClick={resetFontSize}>
          <ListItemIcon><YoutubeSearchedForIcon className={classes.iconColor}
                                                titleAccess="Ikona početne veličine slova"/></ListItemIcon>
          <ListItemText>Početna veličina slova</ListItemText>
        </ListItem>

        <Divider variant="middle"/>

        <ListItem button onClick={() => setTheme(highContrastTheme)}>
          <ListItemIcon><InvertColorsIcon className={classes.iconColor}
                                          titleAccess="Ikona visokog kontrasta"/></ListItemIcon>
          <ListItemText>Visoki kontrast</ListItemText>
        </ListItem>

        <ListItem button onClick={() => setTheme(defaultTheme)}>
          <ListItemIcon><InvertColorsOffIcon className={classes.iconColor} titleAccess="Ikona povratka na zadanu temu"/></ListItemIcon>
          <ListItemText>Zadana tema</ListItemText>
        </ListItem>

        <Divider variant="middle"/>

        <ListItem button onClick={() => {
          setTheme(defaultTheme);
          resetFontSize();
        }}>
          <ListItemIcon><UndoIcon className={classes.iconColor}
                                  titleAccess="Ikona povratka na početne postavke"/></ListItemIcon>
          <ListItemText>Početne postavke</ListItemText>
        </ListItem>
      </List>

    </div>
  );

  return (
    <React.Fragment>
      <Box ml={2}>
        <IconButton aria-label="display more actions" aria-controls="drawer" onClick={handleDrawer}
                    edge="end" color="inherit" className={classes.iconStyle}>
          <AccessibilityNewIcon fontSize="large" titleAccess="Ikona pristupačnosti"/>
        </IconButton>
      </Box>

      <Drawer id="drawer" anchor="right"
              open={open} onClose={handleDrawerClose}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
      >
        {list()}
      </Drawer>
    </React.Fragment>
  );
}