import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import Card from '@material-ui/core/Card';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Zoom from '@material-ui/core/Zoom';
import DialogContentText from '@material-ui/core/DialogContentText';
import LinearProgress from '@material-ui/core/LinearProgress';
import Header from '../Header';
import QuestionCard from './QuestionCard';
import CardContent from '@material-ui/core/CardContent';
import PageContentWrapper from '../PageContentWrapper';
import * as Constants from '../../Constants.js';
import {GroupContext} from '../../App';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    position: 'sticky',
    top: '0',
    zIndex: '2'
  },
  linearProgress: {
    background: theme.name !== 'default-theme' ? '#848442' : '#c4c4c4'
  },
  barColor: {
    background: theme.name !== 'default-theme' ? '#feff26' : 'linear-gradient(to right, #fcb045, #fd1d1d, #833ab4)'
  },
  marginTop: {
    marginTop: theme.spacing(2)
  },
  successButton: {
    backgroundColor: theme.palette.success.main,
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  container: {
    [theme.breakpoints.only('xs')]: {
      padding: 0
    }
  },
  border: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  }
}));

export default function QueryPage({sectorName, sectorDescription, category}) {

  const classes = useStyles();

  const getSectors = React.useContext(GroupContext).getSectors;
  const [completed, setCompleted] = React.useState(0);
  const [answers, setAnswers] = React.useState(new Map());
  const [textAnswers, setTextAnswers] = React.useState(new Map());
  const [required, setRequired] = React.useState([]);
  const [universities, setUniversities] = React.useState([]);
  const [openDialog, setOpenDialog] = React.useState(false);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  React.useEffect(() => {
    if (category.questions.filter(question => question.questionType === "UNIVERSITY_PICKER")) {
      fetch(Constants.GET_UNIVERSITIES)
        .then(response => {
          if (response.ok)
            return response.json()
        })
        .then(data => setUniversities(data))
        .catch(() => setUniversities([]));
    }
  }, [category.questions]);

  const handleExclusiveAnswerToTheQuestion = (questionId, answerId) => {
    if (!answerId) {
      answers.delete(questionId);
      setAnswers(answers);
    } else {
      setAnswers(answers.set(questionId, answerId));
      let index = required.indexOf(questionId.toString());
      if (index > -1) {
        required.splice(index, 1);
        setRequired(prevState => [...prevState]);
      }
    }
    setCompleted(((answers.size + textAnswers.size) / category.questions.length) * 100);
  }

  const handleInclusiveAnswerToTheQuestion = (questionId, answerId) => {
    if (answers.has(questionId.toString())) {
      let array = answers.get(questionId.toString());
      let index = array.indexOf(answerId);
      if (index > -1) {
        array.splice(index, 1);
        setAnswers(answers.set(questionId, array));
        if (array.length === 0) {
          answers.delete(questionId);
          setAnswers(answers);
        }
      } else {
        array.push(answerId);
        setAnswers(answers.set(questionId, array));
      }
    } else {
      setAnswers(answers.set(questionId, [answerId]));
    }

    let index = required.indexOf(questionId.toString());
    if (index > -1) {
      required.splice(index, 1);
      setRequired(prevState => [...prevState]);
    }
    setCompleted(((answers.size + textAnswers.size) / category.questions.length) * 100);
  }

  const handleTextAnswerToTheQuestion = (questionId, text) => {
    if (!text) {
      textAnswers.delete(questionId);
      setTextAnswers(textAnswers);
    } else {
      setTextAnswers(textAnswers.set(questionId, text));
      let index = required.indexOf(questionId.toString());
      if (index > -1) {
        required.splice(index, 1);
        setRequired(prevState => [...prevState]);
      }
    }
    setCompleted(((answers.size + textAnswers.size) / category.questions.length) * 100);
  }

  function onSubmit(e) {
    e.preventDefault();

    let result = category.questions
      .filter(question =>
        (question.required && !answers.has(question.id.toString())) && (question.required && !textAnswers.has(question.id.toString())))
      .map(question => question.id.toString());

    setRequired(result);

    if (result.length !== 0) {
      return;
    }

    let idArray = [];
    answers.forEach((value) => {
      if (Array.isArray(value)) {
        value.forEach(v => idArray.push(v));
      } else {
        idArray.push(value);
      }
    });

    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(idArray)
    };

    let textArray = [];
    textAnswers.forEach((value, key) => textArray.push({questionId: key, text: value}));

    const textOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(textArray)
    }

    fetch(Constants.VOTE, options)
      .then(response => {
        if (response.ok) {
          fetch(Constants.POST_TEXT_ANSWER, textOptions)
            .then(response => {
              if (response.ok) {
                setOpenDialog(true);
                getSectors();
                console.log("Noted!");
              } else {
                console.log("ERROR");
              }
            })
        } else {
          console.log("ERROR");
        }
      })
  }

  const sortQuestions = (questions) => {
    return questions.sort(function (a, b) {
      return a.index - b.index || a.text.localeCompare(b.text);
    });
  }

  return (
    <React.Fragment>
      <div className={classes.root}>
        <LinearProgress variant="determinate" value={completed} className={classes.linearProgress}
                        classes={{barColorPrimary: classes.barColor}}/>
      </div>
      <Header title={sectorName} categoryName={category.name} height={40} randomBg/>

      <PageContentWrapper>
        <Container maxWidth="md" className={classes.container}>
          <form onSubmit={onsubmit}>
            <Grid container spacing={2}>

              {sectorDescription &&
              <Grid item xs={12}>
                <Card raised={true}>
                  <Box border={1} borderRadius={6} className={classes.border}>
                    <Box className="bg-instagram" style={{height: '10px'}}/>
                    <CardContent>
                      <Typography align="center">
                        <em>{sectorDescription}</em>
                      </Typography>
                    </CardContent>
                  </Box>
                </Card>
              </Grid>}

              {category.questions && sortQuestions(category.questions).map((question) =>
                <QuestionCard key={question.id} question={question} universities={universities}
                              requiredAlert={required.includes(question.id.toString())}
                              handleExclusiveAnswerToTheQuestion={handleExclusiveAnswerToTheQuestion}
                              handleTextAnswerToTheQuestion={handleTextAnswerToTheQuestion}
                              handleInclusiveAnswerToTheQuestion={handleInclusiveAnswerToTheQuestion}
                />)
              }
            </Grid>

            <Grid className={classes.marginTop} container justify="center">
              <Button type="submit" variant="contained" color="primary" className={classes.successButton}
                      onClick={onSubmit} endIcon={<DoneAllIcon/>}>
                Predaj odgovore
              </Button>
            </Grid>
          </form>
        </Container>

        <Dialog open={openDialog} fullWidth={true} maxWidth="sm" TransitionComponent={Zoom}
                keepMounted aria-labelledby="sector-dialog" onClose={() => setOpenDialog(false)}>

          <DialogContent>
            <DialogContentText>
              Poštovani, hvala Vam što ste sudjelovali u ovom istraživanju!
            </DialogContentText>
            <DialogContentText>
              Prikupljeni podaci će biti analizirani na grupnoj razini i korišteni u znanstveno - istraživačke svrhe.
            </DialogContentText>
            <DialogContentText>
              U slučaju da želite neke dodatne informacije o rezultatima i istraživanjima vezanima uz ovo područje
              slobodno se javite na <a href="mailto:admin-instedurep@fer.hr">admin-instedurep@fer.hr</a>
            </DialogContentText>
            <DialogContentText>
              Hvala Vam na Vašem doprinosu i želimo Vam svako dobro!
            </DialogContentText>
          </DialogContent>

          <DialogActions>
            <Link to="/" className="text-decoration-none">
              <Button onClick={() => setOpenDialog(false)} color="primary" className={classes.border}>
                Povratak na početnu
              </Button>
            </Link>
          </DialogActions>

        </Dialog>
      </PageContentWrapper>
    </React.Fragment>
  );
}

QueryPage.propTypes = {
  sectorName: PropTypes.string.isRequired,
  sectorDescription: PropTypes.string,
  category: PropTypes.object.isRequired
}
