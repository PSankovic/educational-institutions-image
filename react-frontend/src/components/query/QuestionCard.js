import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import Dropdown from './answerTypes/Dropdown';
import CloseEnded from './answerTypes/CloseEnded';
import LinearScale from './answerTypes/LinearScale';
import MultipleChoice from './answerTypes/MultipleChoice';
import ShortAnswer from './answerTypes/ShortAnswer';
import Paragraph from './answerTypes/Paragraph';
import Checkboxes from './answerTypes/Checkboxes';
import EmojiScale from './answerTypes/EmojiScale';
import UniversityPicker from './answerTypes/UniversityPicker';

const useStyles = makeStyles(theme => ({
  cardContent: {
    padding: theme.spacing(1)
  },
  borderColor: {
    borderColor: theme.palette.border
  }
}));

export default function QuestionCard({
                                       question, universities, requiredAlert, handleExclusiveAnswerToTheQuestion,
                                       handleTextAnswerToTheQuestion, handleInclusiveAnswerToTheQuestion
                                     }) {

  const classes = useStyles();

  const handleExclusiveAnswerChange = (answerId) => {
    handleExclusiveAnswerToTheQuestion(question.id.toString(), answerId);
  }

  const handleInclusiveAnswerChange = (answerId) => {
    handleInclusiveAnswerToTheQuestion(question.id.toString(), answerId);
  }

  const handleTextAnswerChange = (text) => {
    handleTextAnswerToTheQuestion(question.id.toString(), text);
  }

  const sortAnswers = (answers) => {
    return answers.sort(function (a, b) {
      return a.index - b.index;
    });
  }

  return (
    <Grid item xs={12}>
      <Card raised={true}>
        <Box border={1} borderRadius={6} className={classes.borderColor}>
          <CardContent>
            <Box fontWeight="fontWeightBold" display="flex" alignItems="center" pt={1} pb={2} px={1}>
              <Box mr={1}>
                <Typography><b>{question.index}.</b></Typography>
              </Box>
              <Box mr={1}>
                <Typography><b>{question.text}</b></Typography>
              </Box>
              {question.required && <Typography style={{color: red[500]}}><b>*</b></Typography>}
            </Box>

            <Divider/>
            <Box p={1}>
              {question.questionType === "CLOSE_ENDED" &&
              <CloseEnded className={classes.cardContent} answers={sortAnswers(question.answers)}
                          handleAnswerChange={handleExclusiveAnswerChange}/>
              }
              {question.questionType === "SHORT_ANSWER" &&
              <ShortAnswer className={classes.cardContent} handleAnswerChange={handleTextAnswerChange}/>

              }
              {question.questionType === "PARAGRAPH" &&
              <Paragraph className={classes.cardContent} handleAnswerChange={handleTextAnswerChange}/>
              }

              {question.questionType === "MULTIPLE_CHOICE" &&
              <MultipleChoice className={classes.cardContent} answers={sortAnswers(question.answers)}
                              handleAnswerChange={handleExclusiveAnswerChange}/>
              }
              {question.questionType === "CHECKBOXES" &&
              <Checkboxes className={classes.cardContent} answers={sortAnswers(question.answers)}
                          handleAnswerChange={handleInclusiveAnswerChange}/>
              }
              {question.questionType === "DROPDOWN" &&
              <Dropdown className={classes.cardContent} answers={sortAnswers(question.answers)}
                        handleAnswerChange={handleExclusiveAnswerChange}/>
              }
              {question.questionType === "UNIVERSITY_PICKER" &&
              <UniversityPicker className={classes.cardContent} universities={universities}
                                handleAnswerChange={handleTextAnswerChange}/>
              }
              {question.questionType === "EMOJI_SCALE" &&
              <EmojiScale className={classes.cardContent} answers={sortAnswers(question.answers)}
                          handleAnswerChange={handleExclusiveAnswerChange}/>
              }
              {question.questionType === "LINEAR_SCALE" &&
              <LinearScale className={classes.cardContent} answers={sortAnswers(question.answers)}
                           handleAnswerChange={handleExclusiveAnswerChange}/>
              }
            </Box>
            {requiredAlert && <Alert severity="error">Potreban odgovor ovo na pitanje</Alert>}
          </CardContent>
        </Box>
      </Card>
    </Grid>
  );
}

QuestionCard.propTypes = {
  question: PropTypes.object.isRequired,
  universities: PropTypes.array,
  requiredAlert: PropTypes.bool,
  handleExclusiveAnswerToTheQuestion: PropTypes.func.isRequired,
  handleTextAnswerToTheQuestion: PropTypes.func.isRequired,
  handleInclusiveAnswerToTheQuestion: PropTypes.func.isRequired
}