import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  formControl: {
    minWidth: 170
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
}));

const CustomSelect = withStyles((theme) => ({
  icon: {
    color: theme.name !== 'default-theme' ? theme.palette.primary.contrastText : 'default'
  }
}))(Select);

export default function Dropdown({answers, handleAnswerChange}) {

  const classes = useStyles();

  const [answerId, setAnswerId] = React.useState('');

  const handleOnClickChange = (event) => {
    let id = event.target.value;
    setAnswerId(id);
    handleAnswerChange(id);
  }

  return (
    <Grid container justify="center">
      <FormControl className={classes.formControl}>
        <CustomSelect id="dropdownQuestion" value={answerId} displayEmpty autoWidth onChange={handleOnClickChange}>
          <MenuItem value=""><em>Odaberite odgovor</em></MenuItem>
          <Divider/>
          {answers && answers.map((answer) =>
            <MenuItem key={answer.id} value={answer.id.toString()}>{answer.text}</MenuItem>)
          }
        </CustomSelect>
      </FormControl>
    </Grid>
  );
}

Dropdown.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
}
