import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const useStyles = makeStyles((theme) => ({
  width: {
    [theme.breakpoints.only('xs')]: {
      width: '90%'
    },
    [theme.breakpoints.only('sm')]: {
      width: '80%'
    },
    [theme.breakpoints.up('md')]: {
      width: '60%'
    }
  },
  iconColor: {
    color: theme.name !== 'default-theme' ? theme.palette.primary.contrastText : 'default'
  }
}));

export default function UniversityPicker({universities, handleAnswerChange}) {

  const classes = useStyles();
  const [value, setValue] = React.useState('');

  const options = universities.map((university) => {
    const firstLetter = university[0].toString().toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      university,
    };
  })

  const handleValueChange = (event, newValue) => {
    setValue(newValue);
    handleAnswerChange(newValue);
  }

  return (
    <Box display="flex" justifyContent="center">
      <Autocomplete options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                    groupBy={option => option.firstLetter}
                    getOptionLabel={option => option.university}
                    blurOnSelect
                    autoHighlight
                    popupIcon={<ArrowDropDownIcon className={classes.iconColor}/>}
                    freeSolo
                    forcePopupIcon
                    clearOnEscape
                    className={classes.width}
                    renderInput={params => <TextField {...params} label="Naziv visokoškolske ustanove"/>}

                    inputValue={value}
                    onInputChange={((event, newValue) => handleValueChange(event, newValue))}
      />
    </Box>
  );
}

UniversityPicker.propTypes = {
  universities: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
}
