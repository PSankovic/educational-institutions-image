import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

const CustomRadio = withStyles(theme => ({
  root: {
    '&$checked': {
      color: theme.name !== 'default-theme' ? theme.palette.secondary.contrastText : theme.palette.secondary.main
    },
  },
  checked: {}
}))(props => <Radio {...props}/>);

export default function MultipleChoice({answers, handleAnswerChange}) {

  return (
    <FormControl fullWidth component="fieldset">
      <RadioGroup name="question">
        {answers && answers.map((answer) =>
          <FormControlLabel key={answer.id} value={answer.text} label={answer.text}
                            control={<CustomRadio onChange={() => handleAnswerChange(answer.id)}/>}/>)
        }
      </RadioGroup>
    </FormControl>
  );
}

MultipleChoice.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
};
