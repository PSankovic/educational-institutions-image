import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Rating from '@material-ui/lab/Rating';
import {withStyles} from '@material-ui/core/styles';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';

const StyledRating = withStyles((theme) => ({
  iconEmpty: {
    color: theme.name !== 'default-theme' ? '#6b6b10' : 'default'
  }
}))(Rating);

const customIcons = {
  1: {
    icon: <SentimentVeryDissatisfiedIcon fontSize="large"/>,
    label: 'Very Dissatisfied',
  },
  2: {
    icon: <SentimentDissatisfiedIcon fontSize="large"/>,
    label: 'Dissatisfied',
  },
  3: {
    icon: <SentimentSatisfiedIcon fontSize="large"/>,
    label: 'Neutral',
  },
  4: {
    icon: <SentimentSatisfiedAltIcon fontSize="large"/>,
    label: 'Satisfied',
  },
  5: {
    icon: <SentimentVerySatisfiedIcon fontSize="large"/>,
    label: 'Very Satisfied',
  },
};

function IconContainer(props) {
  const {value, ...other} = props;
  return <span {...other}>{customIcons[value].icon}</span>;
}

IconContainer.propTypes = {
  value: PropTypes.number.isRequired,
};

export default function EmojiScale({answers, handleAnswerChange}) {

  const [value, setValue] = React.useState(null);
  const [hover, setHover] = React.useState(-1);

  const handleValueChange = (newValue) => {
    if (newValue) {
      setValue(newValue);
      handleAnswerChange(answers[newValue - 1].id);
    }
  }

  return (
    <React.Fragment>
      <Box display="flex" justifyContent="center" alignItems="center">
        <StyledRating IconContainerComponent={IconContainer} value={value}
                onChange={(event, newValue) => handleValueChange(newValue)}
                onChangeActive={((event, newHover) => setHover(newHover))}/>
      </Box>
      <Box display="flex" justifyContent="center" alignItems="center">
        {!value && hover === -1 && <i>Odaberite odgovor</i>}
        {(value || hover !== -1) && answers[hover !== -1 ? hover - 1 : value - 1].text}
      </Box>
    </React.Fragment>
  );
}

EmojiScale.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
}
