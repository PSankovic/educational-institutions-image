import React from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  border: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  }
}));

export default function CloseEnded({answers, handleAnswerChange}) {

  const classes = useStyles();
  const theme = useTheme();

  const [answerId, setAnswerId] = React.useState('');

  const first = answers[0];
  const second = answers[1];

  const handleOnClickChange = (id) => {
    setAnswerId(id);
    handleAnswerChange(id);
  }

  const getColor = (n) => {
    if (n === 1) {
      if (theme.name === 'default-theme') {
        return answerId !== second.id ? 'primary' : 'default';
      } else {
        if (answerId === '') {
          return 'primary';
        }
        return answerId !== second.id ? 'default' : 'primary';
      }
    } else {
      if (theme.name === 'default-theme') {
        return answerId !== first.id ? 'secondary' : 'default';
      } else {
        if (answerId === '') {
          return 'secondary';
        }
        return answerId !== first.id ? 'default' : 'secondary';
      }
    }
  };

  return (
    <Grid container justify="center">
      <Box mr={1}>
      <Button color={getColor(1)} variant="contained" value={first.id}
              onClick={() => handleOnClickChange(first.id)} className={classes.border}>
        {first.text}
      </Button>
      </Box>

      <Box ml={1}>
      <Button color={getColor(2)} variant="contained" value={second.id}
              onClick={() => handleOnClickChange(second.id)} className={classes.border}>
        {second.text}
      </Button>
      </Box>
    </Grid>
  )
}

CloseEnded.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
}
