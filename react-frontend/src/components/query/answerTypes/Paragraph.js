import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

export default function Paragraph({handleAnswerChange}) {

  const [value, setValue] = React.useState('');

  const handleValueChange = (event) => {
    let value = event.target.value;
    setValue(value);
    handleAnswerChange(value);
  }

  return (
    <TextField type="text" value={value} onChange={handleValueChange} placeholder="Dugi odgovor"
               style={{width: '90%'}} multiline rowsMax={4} inputProps={{maxLength: 256}}/>
  );
}

Paragraph.propTypes = {
  handleAnswerChange: PropTypes.func.isRequired
}
