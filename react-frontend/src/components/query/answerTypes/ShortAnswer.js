import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

export default function ShortAnswer({handleAnswerChange}) {

  const [value, setValue] = React.useState('');

  const handleValueChange = (event) => {
    let value = event.target.value;
    setValue(value);
    handleAnswerChange(value);
  }

  return (
    <TextField type="text" value={value} onChange={handleValueChange} placeholder="Kratki odgovor"
               style={{width: '60%'}} inputProps={{maxLength: 64}}/>
  );
}

ShortAnswer.propTypes = {
  handleAnswerChange: PropTypes.func.isRequired
}
