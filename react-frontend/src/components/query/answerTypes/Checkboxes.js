import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import {withStyles} from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const CustomCheckbox = withStyles((theme) => ({
  root: {
    '&$checked': {
      color: theme.name !== 'default-theme' ? theme.palette.secondary.contrastText : theme.palette.secondary.main
    },
  },
  checked: {}
}))(props => <Checkbox {...props}/>);

export default function Checkboxes({answers, handleAnswerChange}) {

  return (
    <FormControl fullWidth component="fieldset">
      <FormGroup name="question">
        {answers && answers.map((answer) =>
          <FormControlLabel key={answer.id} value={answer.text} label={answer.text}
                            control={<CustomCheckbox onChange={() => handleAnswerChange(answer.id)}/>}/>)
        }
      </FormGroup>
    </FormControl>
  );
}

Checkboxes.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
}
