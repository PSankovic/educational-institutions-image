import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: '0'
  },
  radioButton: {
    padding: '0'
  }
}));

const CustomRadio = withStyles(theme => ({
  root: {
    '&$checked': {
      color: theme.name !== 'default-theme' ? theme.palette.secondary.contrastText : theme.palette.secondary.main
    },
  },
  checked: {}
}))(props => <Radio {...props}/>);

export default function LinearScale({answers, handleAnswerChange}) {

  const classes = useStyles();

  const [answerId, setAnswerId] = React.useState('');

  const lowerLabel = answers[0].text;
  const upperLabel = answers[answers.length - 1].text;

  const handleOnClickChange = (event) => {
    let id = event.target.value;
    setAnswerId(id);
    handleAnswerChange(id);
  }

  return (
    <FormControl fullWidth component="fieldset">
      <RadioGroup name="question" value={answerId} onChange={handleOnClickChange}>
        <Grid container direction="row" alignItems="flex-end" justify="center" spacing={2}>
          <Grid container item xs={3} sm={4} justify="center">
            <Typography align="center">{lowerLabel}</Typography>
          </Grid>
          <Grid item xs={6} sm={4}>
            <Grid container justify="space-between">
              {answers && answers.map((answer, index) =>
                <FormControlLabel key={answer.id} value={answer.id.toString()} className={classes.formControl}
                                  control={<CustomRadio className={classes.radioButton}/>}
                                  label={index + 1}
                                  labelPlacement="top" title={`${index + 1}. - ${answers[index].text}`}/>
              )}
            </Grid>
          </Grid>
          <Grid container item xs={3} sm={4} justify="center">
            <Typography align="center">{upperLabel}</Typography>
          </Grid>
        </Grid>
      </RadioGroup>
    </FormControl>
  );
}

LinearScale.propTypes = {
  answers: PropTypes.array.isRequired,
  handleAnswerChange: PropTypes.func.isRequired
};