import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Zoom from '@material-ui/core/Zoom';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import PersonAddIcon from '@material-ui/icons/PersonAdd';

const useStyles = makeStyles(theme => ({
  buttonCenter: {
    textAlign: 'center'
  },
  button: {
    margin: '1rem 1rem 0',
    padding: '0.5rem'
  },
  buttonIcon: {
    marginRight: '0.5rem'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
  marginTop: {
    marginTop: theme.spacing(1)
  }
}));

export default function AddUserDialog({emails, handleEmail}) {

  const classes = useStyles();

  const settings = {
    maxEmailLength: 256,
    emailRegex: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  };

  const [openDialog, setOpenDialog] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [emailError, setEmailError] = React.useState('');
  const [emailFocus, setEmailFocus] = React.useState(false);

  React.useEffect(() => {
    const checkEmail = () => {
      if (!emailFocus) {
        return ""
      }
      if (email.length > settings.maxEmailLength) {
        return `Maksimalna duljina e-pošte je ${settings.maxEmailLength}`;
      }
      if (!settings.emailRegex.test(email.toLowerCase())) {
        return "Nije valjana adresa e-pošte";
      }
      if (emails.includes(email)) {
        return "Zauzeta adresa e-pošte";
      }
      return "";
    }

    setEmailError(checkEmail());
  }, [emails, email, emailFocus, settings.maxEmailLength, settings.emailRegex]);

  const handleClose = () => {
    setEmail("");
    setEmailError("");
    setEmailFocus(false);
    setOpenDialog(false);
  };

  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  function onSubmit() {
    handleEmail(email);
    handleClose();
  }

  return (
    <React.Fragment>
      <div className={classes.buttonCenter}>
        <Button variant="contained" endIcon={<ContactMailIcon className={classes.buttonIcon}/>} disableRipple
                color="secondary" className={classes.button} onClick={() => setOpenDialog(true)}>
          Dodaj račun
        </Button>
      </div>

      <Dialog open={openDialog} fullWidth={true} maxWidth={"sm"} TransitionComponent={Zoom}
              keepMounted aria-labelledby="sector-dialog" onClose={handleClose}
      >
        <DialogTitle id="sector-dialog">
          <Box alignItems="center" display="flex">
            <PersonAddIcon color={"primary"} style={{marginRight: "0.5rem"}}/>
            Dodajte korisnički račun
          </Box>
        </DialogTitle>

        <form onSubmit={onSubmit}>
          <DialogContent>
            <DialogContentText>
              Upišite e-mail adresu računa koji želite dodati:
            </DialogContentText>
            <div className={classes.form}>
              <TextField autoFocus required id="email" type="email" autoComplete="email" label="E-mail adresa"
                         error={!!emailError}
                         helperText={emailError}
                         value={email}
                         onFocus={() => setEmailFocus(true)}
                         onChange={(e) => setEmail(e.target.value)}
                         inputProps={{maxLength: settings.maxEmailLength}}
              />
            </div>
          </DialogContent>

          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Odustani
            </Button>
            {emailError === "" && !isBlank(email) &&
            <Button type="submit" onClick={onSubmit} color="primary">
              Ok
            </Button>
            }
          </DialogActions>
        </form>
      </Dialog>
    </React.Fragment>
  );
}

AddUserDialog.propTypes = {
  emails: PropTypes.array.isRequired,
  handleEmail: PropTypes.func.isRequired
}
