import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import green from '@material-ui/core/colors/green';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import yellow from '@material-ui/core/colors/yellow';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';
import EmailIcon from '@material-ui/icons/Email';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import {GroupContext} from '../../App';

const useStyles = makeStyles(theme => ({
  root: {
    minWidth: 275
  },
  cardContent: {
    paddingTop: '0'
  },
  heading: {
    display: 'flex',
    padding: theme.spacing(1),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  userDetails: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    flex: '0 0 auto',
    marginRight: theme.spacing(2)
  },
  title: {
    flex: '1 1 auto',
    marginRight: theme.spacing(1)
  },
  removeIcon: {
    alignSelf: 'start',
    marginRight: theme.spacing(1)
  }
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    }
  }
}))(Badge);

export default function UserCard({user, onDeactivation, onReactivation, onRoleChange}) {

  const classes = useStyles();
  const currentUser = React.useContext(GroupContext).currentUser;

  function userNotCurrent() {
    return user.username !== currentUser.username;
  }

  function getUserRole() {
    return user.roles.includes("ADMIN") ? "Administrator" : "Moderator";
  }

  function getBackground() {
    return user.deactivated ? grey[300] : 'inherit';
  }

  return (
    <Grid item xs={12} md={6} lg={4}>
      <Card raised={true} className={classes.root} style={{backgroundColor: getBackground()}}>
        <CardContent className={classes.cardContent} style={{paddingBottom: '0'}}>
          <div className={classes.heading}>
            <div className={classes.userDetails}>
              {!userNotCurrent() &&
              <StyledBadge
                overlap="circle"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                variant="dot"
                className={classes.avatar}
              >
                <Avatar>{user.username.substring(0, 1).toUpperCase()}</Avatar>
              </StyledBadge>}
              {userNotCurrent() &&
              <Avatar className={classes.avatar}>{user.username.substring(0, 1).toUpperCase()}</Avatar>}

              <Typography className={classes.title} variant="h6">{user.username}</Typography>
              {getUserRole() === "Administrator" && <StarIcon style={{color: yellow[700]}}/>}
            </div>
            {userNotCurrent() && !user.deactivated &&
            <IconButton onClick={() => onDeactivation(user.username)}
                        className={classes.removeIcon}><PersonAddDisabledIcon/></IconButton>
            }
            {userNotCurrent() && user.deactivated &&
            <IconButton onClick={() => onReactivation(user.username)}
                        className={classes.removeIcon}><PersonAddIcon/></IconButton>
            }
          </div>

          <Divider/>

          <List>
            <ListItem>
              <ListItemIcon><EmailIcon/></ListItemIcon>
              <ListItemText title="Pošalji email">
                <a href={`mailto:${user.email}`} className="text-decoration-none">{user.email}</a>
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon><LocalOfferIcon/></ListItemIcon>
              <ListItemText>{getUserRole()}</ListItemText>
              {userNotCurrent() && getUserRole() === "Administrator" &&
              <IconButton onClick={() => onRoleChange(user)} style={{color: red[300]}}><ThumbDownIcon/></IconButton>
              }
              {userNotCurrent() && getUserRole() !== "Administrator" &&
              <IconButton onClick={() => onRoleChange(user)} style={{color: green[300]}}><ThumbUpIcon/></IconButton>
              }
            </ListItem>
          </List>

        </CardContent>
      </Card>
    </Grid>
  );
}

UserCard.propTypes = {
  user: PropTypes.object.isRequired,
  onDeactivation: PropTypes.func.isRequired,
  onReactivation: PropTypes.func,
  onRoleChange: PropTypes.func.isRequired
}
