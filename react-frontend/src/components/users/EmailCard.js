import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import EmailIcon from '@material-ui/icons/Email';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles(() => ({
  root: {
    minWidth: 275
  }
}));

export default function EmailCard({email, onDelete}) {

  const classes = useStyles();

  return (
    <Grid item xs={12} md={6} lg={4}>
      <Card raised={true} className={classes.root}>
        <CardContent style={{paddingTop: '0', paddingBottom: '0'}}>
          <List>
            <ListItem disableGutters>
              <ListItemIcon><EmailIcon/></ListItemIcon>
              <ListItemText title="Pošalji email" style={{wordWrap: 'break-word'}}>
                <a href={`mailto:${email}`} className="text-decoration-none">{email}</a>
              </ListItemText>
              <IconButton style={{color: red[500]}} onClick={() => onDelete(email)}><DeleteIcon/></IconButton>
            </ListItem>
          </List>
        </CardContent>
      </Card>
    </Grid>
  );
}

EmailCard.propTypes = {
  email: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired
}
