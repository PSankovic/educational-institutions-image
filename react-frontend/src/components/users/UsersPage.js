import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Header from '../Header';
import UserCard from './UserCard';
import EmailCard from './EmailCard';
import * as Constants from '../../Constants';
import AddUserDialog from './AddUserDialog';
import PageContentWrapper from '../PageContentWrapper';

export default function UsersPage() {

  const [activationEmails, setActivationEmails] = React.useState([]);
  const [users, setUsers] = React.useState([]);

  React.useEffect(() => {
    fetchUsersInfo();
    fetchNonActivatedEmails();
  }, []);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  function fetchUsersInfo() {
    fetch(Constants.GET_ALL_USERS, {credentials: 'include'})
      .then(response => response.json())
      .then(data => setUsers(data));
  }

  function fetchNonActivatedEmails() {
    fetch(Constants.GET_NON_ACTIVATED_EMAILS, {credentials: 'include'})
      .then(response => response.json())
      .then(data => setActivationEmails(data));
  }

  function handleEmail(email) {
    const data = new URLSearchParams();
    data.append("email", email);

    const postData = {
      method: 'POST',
      credentials: 'include',
      body: data
    };

    fetch(Constants.REGISTER_EMAIL, postData)
      .then(response => {
        if (response.ok) {
          fetchNonActivatedEmails();
        } else {
          console.warn("Error while registering email!");
        }
      })
  }

  function handleUserDeactivation(username) {
    fetch(`${Constants.DEACTIVATE_USER}?username=${username}`, {method: 'DELETE', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          fetchUsersInfo();
        } else {
          console.warn("Error deactivating user!");
        }
      })
  }

  function handleUserActivation(username) {
    fetch(`${Constants.REACTIVATE_USER}?username=${username}`, {method: 'PUT', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          fetchUsersInfo();
        } else {
          console.warn("Error reactivating user!");
        }
      })
  }

  function handleUserRoleChange(user) {
    user.roles.includes("ADMIN") ? demoteUser(user.username) : promoteUser(user.username);
  }

  function demoteUser(username) {
    fetch(`${Constants.DEMOTE_USER}?username=${username}`, {method: 'PUT', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          fetchUsersInfo();
        } else {
          console.warn("Cannot demote user!")
        }
      })
  }

  function promoteUser(username) {
    fetch(`${Constants.PROMOTE_USER}?username=${username}`, {method: 'PUT', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          fetchUsersInfo();
        } else {
          console.warn("Cannot promote user!")
        }
      })
  }

  function handleEmailDelete(email) {
    console.log(email);
    fetch(`${Constants.DELETE_EMAIL}?email=${email}`, {method: 'DELETE', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          fetchNonActivatedEmails();
        } else {
          console.warn("Error deleting email!");
        }
      })
  }

  return (
    <React.Fragment>
      <Header title="Imidž Obrazovnih Institucija" subtitle="Završni rad" height={40}/>

      <PageContentWrapper>
        <Box my={2} display="flex" justifyContent="center">
          <Typography variant="h5">Aktivirani korisnici:</Typography>
        </Box>
        <Grid container justify="center" spacing={2}>
          {users && users.map((user) => !user.deactivated &&
            <UserCard key={user.username} user={user} onDeactivation={handleUserDeactivation}
                      onRoleChange={handleUserRoleChange}/>)}
        </Grid>

        <Box>
          <Box mb={2} display="flex" justifyContent="center" mt={4}>
            <Typography variant="h5">Neaktivirani e-mailovi:</Typography>
          </Box>
          <Grid container justify="center" spacing={2}>
            {activationEmails.map((activationEmail) =>
              <EmailCard key={activationEmail.email} email={activationEmail.email} onDelete={handleEmailDelete}/>)}
          </Grid>
        </Box>

        <Box mt={3}>
          <AddUserDialog emails={activationEmails.map(ae => ae.email).concat(users.map(u => u.email))}
                         handleEmail={handleEmail}/>
        </Box>

        <Box my={4} display="flex" justifyContent="center">
          <Typography variant="h5">Deaktivirani korisnici:</Typography>
        </Box>
        <Grid container justify="center" spacing={2}>
          {users && users.map((user) => user.deactivated &&
            <UserCard key={user.username} user={user} onDeactivation={handleUserDeactivation}
                      onReactivation={handleUserActivation} onRoleChange={handleUserRoleChange}/>)}
        </Grid>
      </PageContentWrapper>
    </React.Fragment>
  );
}