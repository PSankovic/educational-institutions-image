import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {useHistory} from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import Popover from '@material-ui/core/Popover';
import Zoom from '@material-ui/core/Zoom';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import {GroupContext} from '../App';

const useStyles = makeStyles(theme => ({
  marginRight: {
    marginRight: theme.spacing(2)
  },
  topbar: {
    position: 'relative'
  },
  menuItem: {}
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  }
}))(Badge);

export default function AppBarMenu() {

  const classes = useStyles();
  const history = useHistory();
  const currentUser = React.useContext(GroupContext).currentUser;
  const onLogout = React.useContext(GroupContext).onLogout;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    onLogout();
    history.push('/');
  }

  return (
    <React.Fragment>
      <StyledBadge
        overlap="circle"
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        variant="dot"
        className={classes.marginRight}
      >
        <Avatar
          aria-label="account of current user"
          aria-controls="menu-appbar"
          aria-haspopup={true}
          onClick={handleMenu}
          color="inherit"
        >
          {currentUser.username.substring(0, 1).toUpperCase()}
        </Avatar>
      </StyledBadge>
      <Popover
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        open={open}
        onClose={handleClose}
        TransitionComponent={Zoom}
      >
        <MenuItem disabled className={classes.menuItem}>
          <em>{currentUser.username}</em>
        </MenuItem>
        <Divider variant="middle"/>
        {onLogout &&
        <div>
          <Divider variant="middle"/>
          <MenuItem onClick={handleLogout}
                    onMouseEnter={(e) => e.target.style.backgroundColor = '#b8bfc3'}
                    onMouseLeave={(e) => e.target.style.backgroundColor = '#ffffff'}
                    className={classes.menuItem}
          >
            Odjavite se
          </MenuItem>
        </div>
        }
      </Popover>
    </React.Fragment>
  );
}