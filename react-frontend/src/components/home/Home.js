import React, {useRef} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import lightBlue from '@material-ui/core/colors/lightBlue';
import SwipeableViews from 'react-swipeable-views';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Collapse from '@material-ui/core/Collapse';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import SvgIcon from '@material-ui/core/SvgIcon';
import BusinessIcon from '@material-ui/icons/Business';
import SchoolIcon from '@material-ui/icons/School';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import {ReactComponent as UniversityIcon} from '../../university-solid.svg';
import Header from '../Header';
import SelectSectorDialog from './SelectSectorDialog';
import PageContentWrapper from '../PageContentWrapper';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: theme.spacing(2)
  },
  button: {
    borderColor: theme.palette.text.textPrimary,
    textTransform: 'none',
    display: 'inline',
    "&:focus": {
      backgroundColor: lightBlue[50]
    }
  },
  learnMoreButton: {
    color: theme.palette.text.textPrimary,
    margin: theme.spacing(2)
  },
  iconMargins: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  textAlign: {
    textAlign: 'center',
    marginBottom: theme.spacing(1)
  },
  control: {
    padding: theme.spacing(2)
  },
  collapse: {
    marginBottom: theme.spacing(2)
  }
}));

export default function Home({loaded}) {

  const [index, setIndex] = React.useState(0);
  const [openDescription, setOpenDescription] = React.useState(false);
  const classes = useStyles();

  const ref = useRef(null);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const scrollToRef = () => {
    ref.current.scrollIntoView({behavior: 'smooth', block: 'start'})
  }

  const handleChangeIndex = (i) => {
    if (index !== i || !openDescription) {
      setOpenDescription(true);
      setIndex(i);
      if (window.innerWidth < 960) {
        scrollToRef();
      }
    } else {
      setOpenDescription(false);
    }
  }

  return (
    <React.Fragment>
      <Header title="Imidž Obrazovnih Institucija" subtitle="Završni rad" height={60}/>

      <PageContentWrapper>
        <Grid container className={classes.root} spacing={3} justify="center">
          <Grid item xs={12} md={4}>

            <Box display="flex" justifyContent="center">
              <Button variant="outlined" onClick={() => handleChangeIndex(0)} className={classes.button}>
                <Grid container justify="center" className={classes.iconMargins}>
                  <BusinessIcon titleAccess="Poslodavci" style={{fontSize: 50}}/>
                </Grid>
                <Grid container justify="center" className={classes.textAlign}>
                  <Typography variant="subtitle1">
                    Uloga imidža visokoobrazovnih ustanova kod poslodavaca u procesu zapošljavanja
                  </Typography>
                  <p/>
                </Grid>
                <Divider/>
                <Box m={1}>
                  <Typography color="primary" variant="button" className={classes.learnMoreButton}>
                    Saznaj više
                  </Typography>
                </Box>
              </Button>
            </Box>

          </Grid>
          <Grid item xs={12} md={4}>

            <Box display="flex" justifyContent="center">
              <Button variant="outlined" onClick={() => handleChangeIndex(1)} className={classes.button}>
                <Grid container justify="center" className={classes.iconMargins}>
                  <SchoolIcon titleAccess="Studenti" style={{fontSize: 50}}/>
                </Grid>
                <Grid container justify="center" className={classes.textAlign}>
                  <Typography variant="subtitle1">
                    Uloga imidža visokoškolskih ustanova kod studenata – budućih ljudskih potencijala.
                  </Typography>
                  <p/>
                </Grid>
                <Divider/>
                <Box m={1}>
                  <Typography color="primary" variant="button" className={classes.learnMoreButton}>
                    Saznaj više
                  </Typography>
                </Box>
              </Button>
            </Box>

          </Grid>
          <Grid item xs={12} md={4}>

            <Box display="flex" justifyContent="center">
              <Button variant="outlined" onClick={() => handleChangeIndex(2)} className={classes.button}>
                <Grid container justify="center" className={classes.iconMargins}>
                  <SvgIcon titleAccess="Visokoobrazovna ustanova" component={UniversityIcon} style={{fontSize: 50}}
                           viewBox="0 0 600 476.6"/>
                </Grid>
                <Grid container justify="center" className={classes.textAlign}>
                  <Typography variant="subtitle1">
                    Koja je uloga imidža fakulteta i koliko on doprinosi pozitivno ili negativno u cijelom procesu
                    odabira
                    zaposlenika.
                  </Typography>
                </Grid>
                <Divider/>
                <Box m={1}>
                  <Typography color="primary" variant="button" className={classes.learnMoreButton}>
                    Saznaj više
                  </Typography>
                </Box>
              </Button>
            </Box>

          </Grid>
        </Grid>

        <Collapse in={openDescription} ref={ref} className={classes.collapse}>
          <SwipeableViews index={index} onChangeIndex={handleChangeIndex} axis="x" animateTransitions={true}>
            <Container maxWidth="md">
              <Alert severity="info" onClose={() => setOpenDescription(false)}>
                <Typography paragraph align="justify">
                  Danas se često govori o tome kako je radi povećanja efikasnosti cjelokupnog gospodarstva potrebno
                  detektirati potrebe tržišta i uklopiti ih u obrazovni sustav.
                </Typography>
                <Typography paragraph align="justify">
                  Ako znamo potrebe koje se javljaju na tržištu kada su u pitanju znanja i vještine budućih ljudskih
                  potencijala koje su potrebne poslodavcima, onda možemo znati i na što je to potrebno staviti naglasak
                  u obrazovnom sustavu kako bi budući ljudski potencijali bili maksimalno pripremljeni za aktivno
                  uključivanje na tržište rada, ali i kako bi svojim znanjima i vještinama zadovoljavali njegove potrebe
                  jer se jedino tako može postići sklad između ponude i potražnje te minimizirati nezaposlenost.
                </Typography>
                <Typography align="justify">
                  Ali, što se događa kad se pred poslodavcem nađu dva potencijalna zaposlenika, koja posjeduju jednaka
                  znanja i vještine, ali stečena na različitim visokoškolskim ustanovama?
                </Typography>
                <Typography align="justify">
                  Kako poslodavac u toj situaciji odlučuje i koju ulogu igra imidž u tom trenutku je ono što Vam ovo
                  programsko rješenje može pokazati.
                </Typography>
              </Alert>
            </Container>

            <Container maxWidth="md">
              <Alert severity="info" onClose={() => setOpenDescription(false)}>
                <Typography paragraph align="justify">
                  Sve je više inicijativa vezanih za povezivanje gospodarstva i obrazovnog sustava pa je prema tome
                  studente potrebno promatrati kao ljudske potencijale akademskog okruženja koji će uskoro postati
                  ljudski potencijali organizacijskog poslovnog okruženja.
                </Typography>
                <Typography paragraph align="justify">
                  Upravo je zato važno prikupljati i analizirati mišljenja i procjene studenata vezana uz različite
                  segmente obrazovnog sustava, a ovo programsko rješenje fokusira se na prikupljanje informacija vezanih
                  za doživljaj imidža visokoškolskih ustanova od strane studenata.
                </Typography>
              </Alert>
            </Container>

            <Container maxWidth="md">
              <Alert severity="info" onClose={() => setOpenDescription(false)}>
                <Typography paragraph align="justify">
                  S obzirom na to da danas postoji značajan broj ponuditelja u segmentu obrazovnih institucija koje
                  pružaju iste ili slične ishode učenja, treba imati u vidu da je upravo imidž komponenta koja često
                  može igrati presudnu ulogu u procesu zapošljavanja.
                </Typography>
                <Typography paragraph align="justify">
                  Ovo programsko rješenje može služiti kao zanimljiva podloga za prikupljanje informacija od strane
                  poslodavaca te povećanje svijesti o tome na koji način se promatraju pojedine obrazovne institucije u
                  poslovnom organizacijskom okruženju kada je u pitanju proces zapošljavanja, ali uz to, može dati uvid
                  u to na koji način studenti procjenjuju imidž fakulteta na kojem studiraju kao i ulogu imidža
                  fakulteta općenito.
                </Typography>
              </Alert>
            </Container>
          </SwipeableViews>
        </Collapse>

        <Box mx={1}>
          <Typography paragraph align="justify">
            Ovo programsko rješenje omogućuje provođenje istraživanja za dobivanje uvida o tome koliki značaj ima imidž
            fakulteta prilikom odabira budućih radnih potencijala kod poslodavaca iz različitih sektora.
          </Typography>
          <Typography paragraph align="justify">
            Informacije koje su vezane za ovaj segment djelovanja itekako mogu pomoći budućim ljudskim potencijalima u
            pronalaženju informacija o tome što je poslodavcima važno i o tome na koji način doživljavaju određene
            segmente obrazovnog sustava, tj. imidže pojedinih fakulteta.
          </Typography>
          <Typography paragraph align="justify">
            Uz to, ovo programsko rješenje može poslužiti i za prikupljanje informacija od poslodavaca o njihovim
            potrebama (vezano za različite segmente poslovanja) te sukladno tome i mogućnost za prilagođavanje drugih
            djelovanja u obrazovnom sustavu u svrhu podizanja kvalitete tržišta rada na obostrano zadovoljstvo
            (poslodavci
            – obrazovni sustav).
          </Typography>
        </Box>

        <SelectSectorDialog/>
      </PageContentWrapper>

      <Snackbar open={!loaded} message={<Typography>Pričekajte da se pokrene server</Typography>}
                action={<CircularProgress color="secondary" size={25}/>}/>

    </React.Fragment>
  );
}

Home.propTypes = {
  loaded: PropTypes.bool.isRequired
}
