import React from 'react';
import {Link} from 'react-router-dom';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import CategoryIcon from '@material-ui/icons/Category';
import Box from '@material-ui/core/Box';
import Zoom from '@material-ui/core/Zoom';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import DialogContentText from '@material-ui/core/DialogContentText';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import {GroupContext} from '../../App';

const useStyles = makeStyles(theme => ({
  buttonCenter: {
    textAlign: 'center'
  },
  button: {
    margin: '1rem 1rem 0',
    padding: '0.5rem',
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  buttonIcon: {
    marginRight: '0.5rem'
  },
  border: {
    color: theme.name !== 'default-theme' ? theme.palette.primary.contrastText : 'inherit',
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4,
    borderColor: theme.palette.border
  },
  inputLabel: {
    color: theme.palette.text.textPrimary,
    "&:focus": {
      color: theme.palette.text.textPrimary
    }
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content'
  },
  formControl: {
    minWidth: 120
  },
  marginTop: {
    marginTop: theme.spacing(1)
  },
  categoryIcon: {
    color: theme.palette.text.textPrimary,
    marginRight: theme.spacing(1)
  }
}));

const CustomSelect = withStyles((theme) => ({
  icon: {
    color: theme.name !== 'default-theme' ? theme.palette.primary.contrastText : 'default'
  }
}))(Select);

export default function SelectSectorDialog() {

  const classes = useStyles();

  const [openDialog, setOpenDialog] = React.useState(false);
  const [selectedSector, setSelectedSector] = React.useState('');
  const [selectedCategory, setSelectedCategory] = React.useState('');
  const generateLink = React.useContext(GroupContext).generateLink;
  const sectors = React.useContext(GroupContext).sectors;

  const handleDialogClose = () => {
    setOpenDialog(false);
  };

  const getSelectSectorText = () => {
    if (selectedCategory === "Poslodavac") {
      return "Molimo Vas da odaberete sektor u kojem djelujete kao poslodavac:";
    }
    return "Molimo Vas da odaberete sektor u kojem ćete djelovati nakon što završite studij:";
  }

  const getSectorDescription = (sectorName) => {
    return sectors.filter(sector => sector.name === sectorName)[0].description;
  }

  return (
    <React.Fragment>
      <Box className={classes.buttonCenter}>
        <Button variant="contained" endIcon={<QuestionAnswerIcon className={classes.buttonIcon}/>}
                color="secondary" className={classes.button} onClick={() => setOpenDialog(true)}>
          Pokreni upitnik
        </Button>
      </Box>

      <Dialog open={openDialog} fullWidth={true} maxWidth="sm" TransitionComponent={Zoom}
              keepMounted aria-labelledby="sector-dialog" onClose={handleDialogClose}
      >
        <DialogTitle id="sector-dialog">
          <Box alignItems="center" display="flex">
            <CategoryIcon color="primary" className={classes.categoryIcon}/>
            Odaberite kategoriju i sektor
          </Box>
        </DialogTitle>

        <DialogContent>

          <DialogContentText>
            Molimo Vas da odaberete jeste li student ili poslodavac:
          </DialogContentText>

          <form className={classes.form}>
            <FormControl className={classes.formControl}>
              <InputLabel className={classes.inputLabel} htmlFor="category">Kategorija</InputLabel>
              <CustomSelect autoFocus value={selectedCategory}
                            onChange={(event) => setSelectedCategory(event.target.value)}
              >
                <MenuItem value="Poslodavac">Poslodavac</MenuItem>
                <MenuItem value="Student">Student</MenuItem>
                )}

              </CustomSelect>
            </FormControl>
          </form>

          <Collapse in={!!selectedCategory}>
            <Box mt={5}>
              <DialogContentText>
                {getSelectSectorText()}
              </DialogContentText>

              <form className={classes.form}>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="sector">Sektor</InputLabel>
                  <CustomSelect autoFocus value={selectedSector}
                          onChange={(event) => setSelectedSector(event.target.value)}
                  >
                    {sectors && sectors.map(sector => <MenuItem key={sector.id}
                                                                value={sector.name}>{sector.name}</MenuItem>)}
                  </CustomSelect>
                </FormControl>
              </form>
              {selectedSector && getSectorDescription(selectedSector) &&
              <Alert className={classes.marginTop} severity="info">
                <Typography variant="subtitle2" color="inherit">{getSectorDescription(selectedSector)}</Typography>
              </Alert>
              }
            </Box>
          </Collapse>
        </DialogContent>

        <DialogActions>
          <Button color="primary" className={classes.border} onClick={handleDialogClose}>
            Odustani
          </Button>
          {selectedCategory && selectedSector &&
          <Link to={`/questions/${generateLink(selectedSector)}/${generateLink(selectedCategory)}`}
                className="text-decoration-none">
            <Button color="primary" className={classes.border} onClick={() => setOpenDialog(false)}>
              Ok
            </Button>
          </Link>
          }
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}