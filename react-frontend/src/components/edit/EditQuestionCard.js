import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import TextField from '@material-ui/core/TextField';
import EditCloseEnded from './answerTypes/EditCloseEnded';
import EditShortAnswer from './answerTypes/EditShortAnswer';
import EditParagraph from './answerTypes/EditParagraph';
import EditMultipleChoice from './answerTypes/EditMultipleChoice';
import EditCheckboxes from './answerTypes/EditCheckboxes';
import EditDropdown from './answerTypes/EditDropdown';
import EditUniversityPicker from './answerTypes/EditUniversityPicker';
import EditEmojiScale from './answerTypes/EditEmojiScale';
import EditLinearScale from './answerTypes/EditLinearScale';

const useStyles = makeStyles(theme => ({
  cardAction: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5)
  },
  borderColor: {
    borderColor: theme.palette.border
  }
}));

export default function EditQuestionCard({question, index, handleNewQuestionUpdate, handleQuestionUpdate, universities}) {

  const classes = useStyles();

  const settings = {
    textLength: 512,
  }

  const [edit, setEdit] = React.useState(false);

  const [text, setText] = React.useState(question.text);
  const [required, setRequired] = React.useState(question.required);

  const sortAnswers = (answers) => {
    return answers.sort(function (a, b) {
      return a.index - b.index || a.name.localeCompare(b.name);
    });
  }

  const [answers, setAnswers] = React.useState(question.answers &&
    sortAnswers(question.answers).map(answer => ({index: answer.index, text: answer.text})));

  function updateQuestion() {
    let array = answers.filter(Boolean).filter(answer => answer.index && answer.text);
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
      newArray.push({index: i + 1, text: array[i].text});
    }
    setAnswers(newArray);

    if (!question.id) {
      handleNewQuestion(newArray);
      return;
    }

    const newQuestion = {
      id: question.id,
      text: text,
      index: index,
      answers: question.answers,
      questionType: question.questionType,
      required: required
    }

    const data = {
      question: newQuestion,
      updatedAnswers: newArray
    };
    handleQuestionUpdate(question.id, data);
    setEdit(false);
  }

  function handleNewQuestion(newArray) {
    const newQuestion = {
      text: text,
      index: index,
      answers: newArray,
      questionType: question.questionType,
      required: required
    }

    handleNewQuestionUpdate(index, newQuestion);
    setEdit(false);
  }

  const handleQuestionTextChange = (event) => {
    setText(event.target.value.trim());
  }

  return (
    <Grid item xs={12}>
      <Card raised={true}>
        <Box border={1} borderRadius={6} className={classes.borderColor}>
          <CardContent style={{paddingBottom: 0}}>

            <Box fontWeight="fontWeightBold" display="flex" alignItems="center" pt={1} pb={2} px={1}>
              <Box mr={1}>
                <Typography variant="inherit">{index}.</Typography>
              </Box>
              {!edit && <Box mr={1}><Typography variant="inherit">{text}</Typography></Box>}
              {edit &&
              <Box style={{width: '100%'}}>
                <TextField inputProps={{maxLength: settings.textLength}} multiline fullWidth type="text"
                           placeholder={`Pitanje ${index}`} defaultValue={text} onChange={handleQuestionTextChange}/>
              </Box>}
              {required && <Typography variant="inherit" style={{color: red[500]}}>*</Typography>}
            </Box>

            <Divider/>

            <Box p={2}>

              {question.questionType === "CLOSE_ENDED" &&
              <EditCloseEnded answers={answers} setAnswers={setAnswers} edit={edit} maxLength={25}/>
              }
              {question.questionType === "SHORT_ANSWER" && <EditShortAnswer edit={edit}/>}
              {question.questionType === "PARAGRAPH" && <EditParagraph edit={edit}/>}
              {question.questionType === "MULTIPLE_CHOICE" &&
              <EditMultipleChoice answers={answers} setAnswers={setAnswers} edit={edit}/>}
              {question.questionType === "CHECKBOXES" &&
              <EditCheckboxes answers={answers} setAnswers={setAnswers} edit={edit}/>}
              {question.questionType === "DROPDOWN" &&
              <EditDropdown answers={answers} setAnswers={setAnswers} edit={edit}/>}
              {question.questionType === "UNIVERSITY_PICKER" &&
              <EditUniversityPicker universities={universities} edit={edit}/>}
              {question.questionType === "EMOJI_SCALE" &&
              <EditEmojiScale answers={sortAnswers(question.answers)} setAnswers={setAnswers} edit={edit}/>}
              {question.questionType === "LINEAR_SCALE" &&
              <EditLinearScale answers={question.answers} setAnswers={setAnswers} edit={edit}/>}

            </Box>
          </CardContent>
          <Divider variant="middle"/>

          <CardActions disableSpacing className={classes.cardAction}>
            {edit && <FormControlLabel control={<Switch checked={required} onChange={() => setRequired(!required)}/>}
                                       label={<i>Potreban odgovor</i>}/>}
            <Box ml="auto">
              {!edit && <IconButton onClick={() => setEdit(true)}><EditIcon titleAccess="Uredi pitanje"/></IconButton>}
              {edit && <IconButton onClick={updateQuestion}><SaveIcon titleAccess="Pohrani promjene"/></IconButton>}
            </Box>
          </CardActions>
        </Box>
      </Card>
    </Grid>
  );
}

EditQuestionCard.propTypes = {
  question: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  handleNewQuestionUpdate: PropTypes.func.isRequired,
  handleQuestionUpdate: PropTypes.func.isRequired,
  universities: PropTypes.array
}
