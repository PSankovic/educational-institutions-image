import React from 'react';
import * as Constants from '../../Constants';
import Grid from '@material-ui/core/Grid';
import Header from '../Header';
import PageContentWrapper from '../PageContentWrapper';
import SectorCard from './SectorCard';
import AddSectorDialog from './AddSectorDialog';
import {GroupContext} from '../../App';

export default function EditSectorsPage() {

  const sectors = React.useContext(GroupContext).sectors;
  const getSectors = React.useContext(GroupContext).getSectors;

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  function handleSector(sector) {
    fetch(`${Constants.ADD_SECTOR}?name=${sector}`, {method: 'POST', credentials: 'include'})
      .then(response => {
        if (response.ok) {
          getSectors();
        } else {
          console.warn("Error adding new sector!");
        }
      });
  }

  return (
    <React.Fragment>
      <Header title="Uređivanje sadržaja" subtitle="Sektori" height={40}/>

      <PageContentWrapper>

        <Grid container justify="center" spacing={2}>
          {sectors && sectors.map(sector =>
            <SectorCard key={sector.id} sector={sector}/>
          )}
        </Grid>

        <AddSectorDialog sectors={sectors.map(s => s.name.toLowerCase())} handleSector={handleSector}/>

      </PageContentWrapper>
    </React.Fragment>
  );
}