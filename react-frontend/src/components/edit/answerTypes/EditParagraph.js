import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

export default function EditParagraph({edit}) {

  const [value, setValue] = React.useState('');

  const handleValueChange = (event) => {
    setValue(event.target.value);
  }

  return (
    <TextField type="text" value={value} placeholder="Dugi odgovor" style={{width: '90%'}} multiline rowsMax={4}
               inputProps={{maxLength: 256}} disabled={edit} onChange={handleValueChange}/>
  );
}

EditParagraph.propTypes = {
  edit: PropTypes.bool.isRequired
}
