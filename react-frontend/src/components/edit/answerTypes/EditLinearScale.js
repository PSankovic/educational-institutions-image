import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles(() => ({
  formControl: {
    margin: '0'
  },
  radioButton: {
    padding: '0'
  }
}));

const CustomRadio = withStyles(theme => ({
  root: {
    '&$checked': {
      color: theme.name !== 'default-theme' ? theme.palette.secondary.contrastText : theme.palette.secondary.main
    },
  },
  checked: {}
}))(props => <Radio {...props}/>);

export default function EditLinearScale({answers, setAnswers, edit}) {

  const classes = useStyles();

  const [labels, setLabels] = React.useState([]);
  const [answerIndex, setAnswerIndex] = React.useState('');

  React.useEffect(() => {
    if (answers.length !== 5) {
      let array = [];
      array.push({index: 1, text: 'U potpunosti se ne slažem'});
      array.push({index: 2, text: 'Uglavnom se ne slažem'});
      array.push({index: 3, text: 'Niti se slažem, niti se ne slažem'});
      array.push({index: 4, text: 'Uglavnom se slažem'});
      array.push({index: 5, text: 'U potpunosti se slažem'});
      setAnswers(array);
      setLabels(array);
    } else {
      setLabels(answers);
    }
  }, []);

  const handleOnClickChange = (event) => {
    setAnswerIndex(event.target.value);
  }

  return (
    <React.Fragment>
      {!edit && <FormControl fullWidth component="fieldset">
        <RadioGroup value={answerIndex} onChange={handleOnClickChange}>
          <Grid container direction="row" alignItems="flex-end" justify="center" spacing={2}>
            <Grid container item xs={3} sm={4} justify="center">
              <Typography align="center">{labels.length === 5 && labels[0].text}</Typography>
            </Grid>
            <Grid item xs={6} sm={4}>
              <Grid container justify="space-between">
                {labels.length === 5 && labels.map((answer, index) =>
                  <FormControlLabel key={answer.index} className={classes.formControl} value={index.toString()}
                                    control={<CustomRadio className={classes.radioButton}/>}
                                    label={index + 1}
                                    labelPlacement="top" title={`${index + 1}. - ${labels[index].text}`}/>
                )}
              </Grid>
            </Grid>
            <Grid container item xs={3} sm={4} justify="center">
              <Typography align="center">{labels.length === 5 && labels[labels.length - 1].text}</Typography>
            </Grid>
          </Grid>
        </RadioGroup>
      </FormControl>}

      {edit && labels && labels.map((answer, index) =>
        <Box key={answer.index} display="flex" alignItems="center">
          <Box mr={2}>
            <Typography color="textSecondary">{index + 1}.</Typography>
          </Box>
          <TextField disabled={edit} type="text" defaultValue={answer.text} fullWidth/>
        </Box>)
      }
    </React.Fragment>
  );
}

EditLinearScale.propTypes = {
  answers: PropTypes.array.isRequired,
  setAnswers: PropTypes.func.isRequired,
  edit: PropTypes.bool.isRequired
}
