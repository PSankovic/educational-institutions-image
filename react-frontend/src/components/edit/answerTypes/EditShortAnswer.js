import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

export default function EditShortAnswer({edit}) {

  const [value, setValue] = React.useState('');

  const handleValueChange = (event) => {
    setValue(event.target.value);
  }

  return (
    <TextField type="text" value={value} placeholder="Kratki odgovor" style={{width: '60%'}} disabled={edit}
               inputProps={{maxLength: 64}} onChange={handleValueChange}/>
  );
}

EditShortAnswer.propTypes = {
  edit: PropTypes.bool.isRequired
}
