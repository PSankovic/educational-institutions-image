import React from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Filter1Icon from '@material-ui/icons/Filter1';
import Filter2Icon from '@material-ui/icons/Filter2';

const useStyles = makeStyles(theme => ({
  border: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  iconMargin: {
    marginRight: theme.spacing(2)
  }
}));

export default function EditCloseEnded({answers, setAnswers, edit, maxLength}) {

  const classes = useStyles();
  const theme = useTheme();

  const [answerText, setAnswerText] = React.useState('');

  const first = answers[0] ? {index: 1, text: answers[0].text} : {index: 1, text: ''};
  const second = answers[1] ? {index: 2, text: answers[1].text} : {index: 2, text: ''};

  React.useEffect(() => {
    let array = [...answers];
    if (array.length === 0) {
      array.push({index: 1, text: ''});
    }
    if (array.length === 1) {
      array.push({index: 2, text: ''});
    }
    setAnswers(array);
    // eslint-disable-next-line
  }, [])

  const handleOnClickChange = (text) => {
    setAnswerText(text);
  }

  const getColor = (n) => {
    if (n === 1) {
      if (theme.name === 'default-theme') {
        return answerText !== second.text ? 'primary' : 'default';
      } else {
        if (answerText === '') {
          return 'primary';
        }
        return answerText !== second.text ? 'default' : 'primary';
      }
    } else {
      if (theme.name === 'default-theme') {
        return answerText !== first.text ? 'secondary' : 'default';
      } else {
        if (answerText === '') {
          return 'secondary';
        }
        return answerText !== first.text ? 'default' : 'secondary';
      }
    }
  };

  const handleAnswerTextChange = (event, index) => {
    let array = [...answers];
    array[index] = {index: index + 1, text: event.target.value.trim()};
    setAnswers([...array]);
  }

  return (
    <React.Fragment>
      {!edit &&
      <Grid container justify="center">
        <Box mr={1}>
          <Button color={getColor(1)} variant="contained" value={first.text}
                  onClick={() => handleOnClickChange(first.text)} className={classes.border}>
            {first.text}
          </Button>
        </Box>

        <Box mr={1}>
          <Button color={getColor(2)} variant="contained" value={second.text}
                  onClick={() => handleOnClickChange(second.text)} className={classes.border}>
            {second.text}
          </Button>
        </Box>
      </Grid>}

      {edit &&
      <Grid container alignItems="center" spacing={1}>
        <Grid item xs={12} sm={6}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Filter1Icon className={classes.iconMargin}/>
            <TextField inputProps={{maxLength: maxLength}} type="text" placeholder={'Odgovor 1'}
                       defaultValue={first.text} onChange={(event) => handleAnswerTextChange(event, 0)}/>
          </Box>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Filter2Icon className={classes.iconMargin}/>
            <TextField inputProps={{maxLength: maxLength}} type="text" placeholder={'Odgovor 2'}
                       defaultValue={second.text} onChange={(event) => handleAnswerTextChange(event, 1)}/>
          </Box>
        </Grid>
      </Grid>
      }
    </React.Fragment>
  )
}

EditCloseEnded.propTypes = {
  answers: PropTypes.array.isRequired,
  setAnswers: PropTypes.func.isRequired,
  maxLength: PropTypes.number,
  edit: PropTypes.bool.isRequired
}
