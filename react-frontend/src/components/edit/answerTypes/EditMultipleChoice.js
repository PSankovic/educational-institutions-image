import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

const CustomRadio = withStyles(theme => ({
  root: {
    '&$checked': {
      color: theme.name !== 'default-theme' ? theme.palette.secondary.contrastText : theme.palette.secondary.main
    },
  },
  checked: {}
}))(props => <Radio {...props}/>);

export default function EditMultipleChoice({answers, setAnswers, maxLength, edit}) {

  const addAnswer = () => {
    if (answers.filter(answer => !answer.text).length > 0) {
      return;
    }
    let array = [...answers];
    const index = array.length > 0 ? array[array.length - 1].index + 1 : 1;
    array.push({index: index, text: ''});
    setAnswers(array);
  }

  const handleTextChange = (event, index) => {
    let array = [...answers];
    array[index] = {index: array[index].index, text: event.target.value.trim()};
    setAnswers([...array]);
  }

  const deleteAnswer = (index) => {
    let array = [...answers];
    array.splice(index, 1);
    setAnswers(array);
  }

  const nonEditAnswers =
    <FormControl fullWidth component="fieldset">
      <RadioGroup name="question">
        {answers && answers.map((answer, index) =>
          <FormControlLabel key={index} value={answer.text} control={<CustomRadio/>} label={answer.text}/>)
        }
      </RadioGroup>
    </FormControl>

  return (
    <React.Fragment>
      {!edit && nonEditAnswers}
      {edit && answers && answers.map((answer, index) =>
        <Box key={answer.index} display="flex" alignItems="center">
          <CustomRadio disabled={true} checked={false}/>
          <TextField inputProps={{maxLength: maxLength}} multiline fullWidth type="text"
                     placeholder={`Odgovor ${index + 1}`}
                     defaultValue={answer.text} onChange={(event) => handleTextChange(event, index)}/>
          <Box ml="auto"><IconButton onClick={() => deleteAnswer(index)}><CloseIcon titleAccess="Izbriši odgovor"/></IconButton></Box>
        </Box>
      )}
      {edit &&
      <Box>
        <Box display="flex" alignItems="center">
          <IconButton style={{padding: '9px'}} onClick={addAnswer}><AddIcon titleAccess="Dodaj odgovor"/></IconButton>
          <Typography><i>Dodaj odgovor</i></Typography>
        </Box>
      </Box>
      }
    </React.Fragment>
  );
}

EditMultipleChoice.propTypes = {
  answers: PropTypes.array.isRequired,
  setAnswers: PropTypes.func.isRequired,
  maxLength: PropTypes.number,
  edit: PropTypes.bool.isRequired
}
