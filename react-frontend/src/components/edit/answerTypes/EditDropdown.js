import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import ListIcon from '@material-ui/icons/List';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  formControl: {
    minWidth: 170
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  iconMargin: {
    marginRight: theme.spacing(1)
  }
}));

const CustomSelect = withStyles((theme) => ({
  icon: {
    color: theme.name !== 'default-theme' ? theme.palette.primary.contrastText : 'default'
  }
}))(Select);

export default function EditDropdown({answers, setAnswers, maxLength, edit}) {

  const classes = useStyles();

  const [answerText, setAnswerText] = React.useState('');

  const addAnswer = () => {
    if (answers.filter(answer => !answer.text).length > 0) {
      return;
    }
    let array = [...answers];
    const index = array.length > 0 ? array[array.length - 1].index + 1 : 1;
    array.push({index: index, text: ''});
    setAnswers(array);
  }

  const handleTextChange = (event, index) => {
    let array = [...answers];
    array[index] = {index: array[index].index, text: event.target.value.trim()};
    setAnswers([...array]);
  }

  const deleteAnswer = (index) => {
    let array = [...answers];
    array.splice(index, 1);
    setAnswers(array);
  }

  const handleOnClickChange = (event) => {
    setAnswerText(event.target.value);
  }

  const nonEditAnswers =
    <Grid container justify="center">
      <FormControl className={classes.formControl}>
        <CustomSelect id="dropdownQuestion" value={answerText} displayEmpty autoWidth onChange={handleOnClickChange}>
          <MenuItem value=""><em>Odaberite odgovor</em></MenuItem>
          <Divider/>
          {answers && answers.map((answer, index) =>
            <MenuItem key={index} value={answer.text}>{answer.text}</MenuItem>)
          }
        </CustomSelect>
      </FormControl>
    </Grid>

  return (
    <React.Fragment>
      {!edit && nonEditAnswers}
      {edit && answers && answers.map((answer, index) =>
        <Box key={answer.index} display="flex" alignItems="center">
          <ListIcon titleAccess={`Odgovor ${index + 1}`} className={classes.iconMargin}/>
          <TextField inputProps={{maxLength: maxLength}} multiline fullWidth type="text"
                     placeholder={`Odgovor ${index + 1}`}
                     defaultValue={answer.text} onChange={(event) => handleTextChange(event, index)}/>
          <Box ml="auto"><IconButton onClick={() => deleteAnswer(index)}><CloseIcon titleAccess="Izbriši odgovor"/></IconButton></Box>
        </Box>
      )}
      {edit &&
      <Box>
        <Box display="flex" alignItems="center">
          <IconButton style={{padding: '9px'}} onClick={addAnswer}><AddIcon titleAccess="Dodaj odgovor"/></IconButton>
          <Typography><i>Dodaj odgovor</i></Typography>
        </Box>
      </Box>
      }
    </React.Fragment>
  );
}

EditDropdown.propTypes = {
  answers: PropTypes.array.isRequired,
  setAnswers: PropTypes.func.isRequired,
  maxLength: PropTypes.number,
  edit: PropTypes.bool.isRequired
}
