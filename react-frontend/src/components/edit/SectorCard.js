import React from 'react';
import {Link} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import * as Constants from '../../Constants';
import clsx from 'clsx';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import SaveIcon from '@material-ui/icons/Save';
import red from '@material-ui/core/colors/red';
import EditIcon from '@material-ui/icons/Edit';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {GroupContext} from '../../App';

const useStyles = makeStyles((theme) => ({
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  deleteButton: {
    color: red[300]
  },
  chip: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    boxSizing: 'content-box'
  },
  textMargin: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}));

export default function SectorCard({sector}) {

  const classes = useStyles();

  const generateLink = React.useContext(GroupContext).generateLink;
  const getSectors = React.useContext(GroupContext).getSectors;
  const [expanded, setExpanded] = React.useState(false);
  const [snackbarOpen, setSnackbarOpen] = React.useState(false);
  const [alertText, setAlertText] = React.useState('');
  const [alertSeverity, setAlertSeverity] = React.useState('');
  const [edit, setEdit] = React.useState(false);

  const [name, setName] = React.useState(sector.name.toString());
  const [description, setDescription] = React.useState(sector.description ? sector.description.toString() : '');

  const settings = {
    nameLength: 128,
    descriptionLength: 512
  }

  function updateSector() {
    if (name === sector.name && (description === sector.description || (!sector.description && description === ''))) {
      setEdit(false);
      return;
    }

    const data = new URLSearchParams();
    data.append("id", sector.id);
    data.append("name", name);
    data.append("description", description);

    const fetchData = {
      method: 'PUT',
      credentials: 'include',
      body: data
    };

    fetch(Constants.UPDATE_SECTOR_NAME_DESCRIPTION, fetchData)
      .then(response => {
        if (response.ok) {
          setAlertSeverity("success");
          setAlertText("Ime i opis sektora uspješno ažurirani!");
        } else {
          setAlertSeverity("error");
          setAlertText("Neuspjelo ažuriranje imena i opisa sektora!");
        }
      })
      .then(() => getSectors())
      .then(() => handleEditClick())
      .then(() => setSnackbarOpen(name !== sector.name || description !== sector.description));
  }

  const handleExpandClick = () => {
    setExpanded(!expanded);
  }

  const handleEditClick = () => {
    setEdit(!edit);
  }

  const handleNameChange = (event) => {
    setName(event.target.value.trim());
  }

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value.trim());
  }

  const avatar = <Avatar>{sector.name.substring(0, 1).toUpperCase()}</Avatar>;

  const titleComponent = () => {
    if (edit) {
      return <TextField inputProps={{maxLength: settings.nameLength}} fullWidth type="text" defaultValue={sector.name}
                        onChange={handleNameChange} placeholder="Naziv sektora"/>;
    }
    return <Typography color="textPrimary" variant="h6">{sector.name}</Typography>;
  }

  return (
    <Grid item xs={12} md={6}>
      <Card raised={true}>

        <CardHeader title={titleComponent()} avatar={avatar}/>
        <Divider variant="middle"/>
        <CardContent style={{paddingBottom: '0'}}>
          {!edit &&
          <Typography color="textSecondary" variant="body2">{sector.description ? sector.description :
            <i>Nema opisa</i>}</Typography>}
          {edit &&
          <TextField inputProps={{maxLength: settings.descriptionLength}} multiline fullWidth type="text"
                     placeholder="Opis sektora"
                     defaultValue={sector.description} onChange={handleDescriptionChange}/>}
        </CardContent>
        <CardActions disableSpacing>
          {!edit &&
          <IconButton onClick={handleEditClick}><EditIcon/></IconButton>}
          {edit &&
          <IconButton onClick={updateSector}><SaveIcon/></IconButton>}
          <IconButton onClick={handleExpandClick} aria-expanded={expanded} aria-label="show more"
                      className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded
                      })}>
            <ExpandMoreIcon/>
          </IconButton>
        </CardActions>

        <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={() => setSnackbarOpen(false)}>
          <Alert severity={alertSeverity} variant="filled" onClose={() => setSnackbarOpen(false)}>
            {alertText}
          </Alert>
        </Snackbar>

        <Collapse in={expanded} timeout="auto">
          <Divider variant="middle"/>
          <CardContent>
            <Grid container justify="space-evenly" spacing={2}>
              {sector.categories && sector.categories.map((category) =>
                <Grid key={category.id} item xs={12} sm={6} md={4}>
                  <Box justifyContent="center" display="flex">
                    <Chip className={classes.chip} color="primary" onDelete={() => {}}
                          deleteIcon={<Link
                            to={`/edit-questions/${generateLink(sector.name)}/${generateLink(category.name)}`}>
                            <EditIcon titleAccess="Uredi pitanja"/></Link>}
                          label={<Typography className={classes.textMargin}
                                             variant="body2">{category.name}</Typography>}/>
                  </Box>
                </Grid>
              )}
            </Grid>

          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
}