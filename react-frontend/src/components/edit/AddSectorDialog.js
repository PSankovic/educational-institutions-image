import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Zoom from '@material-ui/core/Zoom';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import CategoryIcon from '@material-ui/icons/Category';

const useStyles = makeStyles(theme => ({
  button: {
    margin: '1rem 1rem 0',
    padding: '0.5rem'
  },
  buttonIcon: {
    marginRight: '0.5rem'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
  marginTop: {
    marginTop: theme.spacing(1)
  }
}));

export default function AddSectorDialog({sectors, handleSector}) {

  const classes = useStyles();

  const settings = {
    maxSectorNameLength: 128
  }

  const [openDialog, setOpenDialog] = React.useState(false);
  const [sector, setSector] = React.useState('');
  const [formError, setFormError] = React.useState('');

  React.useEffect(() => {
    const checkSector = () => {
      if (sectors.includes(sector.toLowerCase())) {
        return "Uneseni sektor već postoji"
      }
      return "";
    }

    setFormError(checkSector());
  }, [sector, sectors]);

  const handleClose = () => {
    setSector("");
    setFormError("");
    setOpenDialog(false);
  };

  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  function onSubmit() {
    handleSector(sector);
    handleClose();
  }

  return (
    <React.Fragment>
      <Box display="flex" justifyContent="center" mt={2}>
        <Button variant="contained" endIcon={<CategoryIcon className={classes.buttonIcon}/>} disableRipple
                color="secondary" className={classes.button} onClick={() => setOpenDialog(true)}>
          Dodaj sektor
        </Button>
      </Box>

      <Dialog open={openDialog} fullWidth={true} maxWidth={"sm"} TransitionComponent={Zoom}
              keepMounted aria-labelledby="sector-dialog" onClose={handleClose}
      >
        <DialogTitle id="sector-dialog">
          <Box alignItems="center" display="flex">
            <CategoryIcon color={"primary"} style={{marginRight: "0.5rem"}}/>
            Dodajte sektor
          </Box>
        </DialogTitle>

        <form onSubmit={onSubmit}>
          <DialogContent>
            <DialogContentText>
              Upišite ime sektora koji želite dodati:
            </DialogContentText>
            <div className={classes.form}>
              <TextField autoFocus required id="text" type="text" autoComplete="text" label="Ime sektora" inputMode="text"
                         inputProps={{maxLength: settings.maxSectorNameLength}}
                         error={!!formError}
                         helperText={formError}
                         value={sector}
                         onChange={(e) => setSector(e.target.value)}
              />
            </div>
          </DialogContent>

          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Odustani
            </Button>
            {formError === "" && !isBlank(sector) &&
            <Button type="submit" onClick={onSubmit} color="primary">
              Ok
            </Button>
            }
          </DialogActions>
        </form>
      </Dialog>
    </React.Fragment>
  );
}

AddSectorDialog.propTypes = {
  sectors: PropTypes.array.isRequired,
  handleSector: PropTypes.func.isRequired
};