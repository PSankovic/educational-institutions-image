import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SvgIcon from '@material-ui/core/SvgIcon';
import SubjectIcon from '@material-ui/icons/Subject';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ShortTextIcon from '@material-ui/icons/ShortText';
import LinearScaleIcon from '@material-ui/icons/LinearScale';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
import {ReactComponent as UniversityIcon} from '../../university-solid.svg';

const useStyles = makeStyles((theme) => ({
  speedDial: {
    position: 'fixed',
    bottom: theme.spacing(4),
    right: theme.spacing(4)
  },
  backdrop: {
    zIndex: 1
  }
}));

const actions = [
  {id: 'LINEAR_SCALE', icon: <LinearScaleIcon/>, name: 'Intervalna skala'},
  {id: 'EMOJI_SCALE', icon: <EmojiEmotionsIcon/>, name: 'Skala emotikona'},
  {id: 'UNIVERSITY_PICKER', icon: <SvgIcon component={UniversityIcon} style={{fontSize: 25, marginLeft: '5%', marginBottom: '5%'}}
                                           viewBox="0 0 600 476.6"/>, name: 'Izbor obrazovnih ustanova'},
  {id: 'DROPDOWN', icon: <ArrowDropDownCircleIcon/>, name: 'Padajući izbornik'},
  {id: 'CHECKBOXES', icon: <CheckBoxIcon/>, name: 'Odabirni okviri'},
  {id: 'MULTIPLE_CHOICE', icon: <RadioButtonCheckedIcon/>, name: 'Višestruki izbor'},
  {id: 'PARAGRAPH', icon: <SubjectIcon/>, name: 'Dugi odgovor'},
  {id: 'SHORT_ANSWER', icon: <ShortTextIcon/>, name: 'Kratki odgovor'},
  {id: 'CLOSE_ENDED', icon: <ThumbsUpDownIcon/>, name: 'Zatvoreni odgovori'}
];

export default function SpeedDialButton({addQuestionType}) {

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleActionClick = (type) => {
    addQuestionType(type);
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Backdrop open={open} className={classes.backdrop}/>
      <SpeedDial ariaLabel="question-types" open={open} icon={<SpeedDialIcon/>} className={classes.speedDial}
                 onClose={() => setOpen(false)} onOpen={() => setOpen(true)}>
        {actions.map((action) => (
          <SpeedDialAction key={action.name} icon={action.icon} tooltipTitle={action.name} id={action.id}
                           onClick={() => handleActionClick(action.id)} title={action.name}/>
        ))}
      </SpeedDial>
    </React.Fragment>
  );
}

SpeedDialButton.propTypes = {
  addQuestionType: PropTypes.func.isRequired
}
