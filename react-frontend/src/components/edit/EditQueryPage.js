import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import green from '@material-ui/core/colors/green';
import Header from '../Header';
import EditQuestionCard from './EditQuestionCard';
import PageContentWrapper from '../PageContentWrapper';
import SpeedDialButton from './SpeedDialButton';
import * as Constants from '../../Constants';

export default function EditQueryPage({sectorName, category}) {

  const [questions, setQuestions] = React.useState([]);
  const [newQuestions, setNewQuestions] = React.useState(new Map());
  const [updatedQuestions, setUpdatedQuestions] = React.useState(new Map());
  const [universities, setUniversities] = React.useState([]);

  const sortQuestions = (questions) => {
    return questions.sort(function (a, b) {
      return a.index - b.index;
    });
  }

  React.useEffect(() => {
    setQuestions(sortQuestions(category.questions));
  }, [])

  React.useEffect(() => {
    if (category.questions.filter(question => question.questionType === "UNIVERSITY_PICKER")) {
      fetch(Constants.GET_UNIVERSITIES)
        .then(response => {
          if (response.ok)
            return response.json()
        })
        .then(data => setUniversities(data))
        .catch(() => setUniversities([]));
    }
  }, [category.questions]);

  function addQuestionType(type) {
    const question = {
      text: '',
      index: questions.length + 1,
      answers: [],
      questionType: type,
      required: false,
      deleted: false
    }
    let array = [...questions];
    array.push(question);
    setQuestions(array);
  }

  function handleQuestionUpdate(id, data) {
    setUpdatedQuestions(updatedQuestions.set(id, data));
  }

  function handleNewQuestionUpdate(index, data) {
    setNewQuestions(newQuestions.set(index, data));
    console.log(index, data);
  }

  function onSubmit(e) {
    e.preventDefault();

    const putData = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify(Array.from(updatedQuestions.values()))
    };

    const postData = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify(Array.from(newQuestions.values()))
    }

    console.log(Array.from(updatedQuestions.values()));
    console.log(Array.from(newQuestions.values()));

    fetch(Constants.UPDATE_QUESTION, putData)
      .then(() => fetch(`${Constants.POST_QUESTIONS}?id=${category.id}`, postData)
        .then(() => window.location.reload())
      );
  }

  return (
    <React.Fragment>
      <Header title={sectorName} categoryName={category.name} height={40} randomBg/>

      <PageContentWrapper>
        <Container maxWidth="md">
          <form onSubmit={onsubmit}>
            <Grid container spacing={2}>

              {questions && questions.map((question) =>
                <EditQuestionCard key={question.index} index={question.index} question={question}
                                  handleNewQuestionUpdate={handleNewQuestionUpdate}
                                  handleQuestionUpdate={handleQuestionUpdate} universities={universities}/>)}

            </Grid>

            <Grid container justify="center">
              <Box mt={2}>
                <Button type="submit" variant="contained" color="primary"
                        onClick={onSubmit} endIcon={<DoneAllIcon/>} style={{backgroundColor: green[600]}}>
                  Potvrdi promjene
                </Button>
              </Box>
            </Grid>
          </form>

        </Container>

        <SpeedDialButton addQuestionType={addQuestionType}/>

      </PageContentWrapper>
    </React.Fragment>
  )
}

EditQueryPage.propTypes = {
  sectorName: PropTypes.string.isRequired,
  category: PropTypes.object.isRequired
}
