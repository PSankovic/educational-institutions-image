import React from 'react';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PeopleIcon from '@material-ui/icons/People';
import PollIcon from '@material-ui/icons/Poll';
import EditIcon from '@material-ui/icons/Edit';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import {GroupContext} from '../App';

const useStyles = makeStyles(theme => ({
  list: {
    width: 240,
    height: '100vh'
  },
  nestedList: {
    padding: theme.spacing(0.5, 4, 0.5, 4)
  },
  background: {
    background: theme.palette.background.sidebar
  },
  iconColor: {
    color: theme.palette.text.textDefault
  }
}));

export default function SidebarMenu() {

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [openResult, setOpenResult] = React.useState(false);
  const currentUser = React.useContext(GroupContext).currentUser;
  const sectors = React.useContext(GroupContext).sectors;
  const generateLink = React.useContext(GroupContext).generateLink;

  const handleDrawer = () => setOpen(!open);
  const handleDrawerClose = () => setOpen(false);
  const handleKeyDown = (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    handleDrawerClose();
  };

  const handleOpenResult = () => setOpenResult(!openResult);

  const list = () => (
    <div role="presentation" onKeyDown={handleKeyDown}
         className={`${clsx(classes.list)} ${classes.background}`}>
      <List subheader={<ListSubheader component="div">Odaberite opciju</ListSubheader>}>
        <Link to="/" className="text-decoration-none">
          <ListItem button onClick={handleDrawerClose}>
            <ListItemIcon><HomeIcon className={classes.iconColor}/></ListItemIcon>
            <ListItemText>Početna</ListItemText>
          </ListItem>
        </Link>

        <ListItem button onClick={handleOpenResult}>
          <ListItemIcon><PollIcon className={classes.iconColor}/></ListItemIcon>
          <ListItemText>Rezultati</ListItemText>
          {openResult ? <ExpandLess/> : <ExpandMore/>}
        </ListItem>

        <Collapse in={openResult} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {sectors && sectors.map((sector) =>
              <Link key={sector.id} to={`/results/${generateLink(sector.name)}`} className="text-decoration-none">
                <ListItem button className={classes.nestedList}>
                  <ListItemIcon><ArrowRightIcon className={classes.iconColor}/></ListItemIcon>
                  <ListItemText primary={sector.name}/>
                </ListItem>
              </Link>
            )}
          </List>
        </Collapse>

        {currentUser.roles.includes("ADMIN") &&
          <Link to="/users" className="text-decoration-none">
            <ListItem button onClick={handleDrawerClose}>
              <ListItemIcon><PeopleIcon className={classes.iconColor}/></ListItemIcon>
              <ListItemText>Korisnici</ListItemText>
            </ListItem>
          </Link>
        }

        <Link to="/edit" className="text-decoration-none">
          <ListItem button onClick={handleDrawerClose}>
            <ListItemIcon><EditIcon className={classes.iconColor}/></ListItemIcon>
            <ListItemText>Uredi sadržaj</ListItemText>
          </ListItem>
        </Link>
      </List>
    </div>
  );

  return (
    <React.Fragment>
      <IconButton edge="start" color="inherit" aria-label="open drawer" onClick={handleDrawer}>
        <MenuIcon fontSize="large"/>
      </IconButton>

      <Drawer id="menu-drawer" anchor="left" open={open} onClose={handleDrawerClose}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
      >
        {list()}
      </Drawer>

    </React.Fragment>
  );
}