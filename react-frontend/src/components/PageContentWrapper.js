import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Footer from './Footer';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: -theme.spacing(6),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(1)
    },
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(4)
    },
    boxShadow: '0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    zIndex: 1,
    position: 'relative',
    borderRadius: '10px'
  },
  container: {
    [theme.breakpoints.only('xs')]: {
      padding: theme.spacing(1)
    }
  }
}));

export default function PageContentWrapper({children}) {

  const classes = useStyles();

  return (
    <React.Fragment>
      <Container className={classes.container}>
        <Paper className={classes.paper}>
          {children}
        </Paper>
      </Container>

      <Footer/>
    </React.Fragment>
  );
}

PageContentWrapper.propTypes = {
  children: PropTypes.any.isRequired
};