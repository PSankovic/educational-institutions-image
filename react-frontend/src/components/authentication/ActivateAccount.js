import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {Link, useHistory} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import LockIcon from '@material-ui/icons/Lock';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import * as Constants from '../../Constants';
import AuthenticationPageWrapper from './AuthenticationPageWrapper';

const useStyles = makeStyles(theme => ({
  alert: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  color: {
    color: theme.name === 'default-theme' ? 'default' : theme.palette.primary.contrastText
  }
}));

export default function ActivateAccount({onLogin}) {

  const history = useHistory();
  const classes = useStyles();

  const settings = {
    minPasswordLength: 4,
    maxPasswordLength: 64,
    minUsernameLength: 2,
    maxUsernameLength: 32,
    maxEmailLength: 256,
    emailRegex: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  };

  const [form, setForm] = React.useState({
    username: '',
    email: '',
    password: '',
    confirmPassword: ''
  });
  const [showPassword, setShowPassword] = React.useState(false);
  const [failedRegister, setFailedRegister] = React.useState(false);
  const [errors, setErrors] = React.useState({});
  const [fieldSelectedAtLeastOnce, setSelectedField] = React.useState({
    usernameB: false,
    emailB: false,
    passwordB: false,
    confirmPasswordB: false,
  });

  React.useEffect(() => {
    setErrors(() => getErrors());
    // eslint-disable-next-line
  }, [form]);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  function onSubmit(e) {
    e.preventDefault();

    const registrationData = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(form)
    };

    const data = new URLSearchParams();
    data.append('username', form.username);
    data.append('password', form.password);

    const loginData = {
      method: 'POST',
      credentials: 'include',
      body: data
    }

    fetch(Constants.REGISTER, registrationData)
      .then(response => {
        if (response.ok) {
          login(loginData);
        } else {
          setFailedRegister(true);
        }
      }).catch(() => console.warn("Account activation error"))
  }

  function login(loginData) {
    fetch(Constants.LOGIN, loginData)
      .then(response => {
        if (response.url.endsWith("?error")) {
          console.warn("?error occured");
        } else {
          console.log("Successfully logged in:", form.username);
          history.push("/");
          onLogin();
        }
      }).catch(() => console.warn("Login Error"));
  }

  function getErrors() {
    const newErrors = {};
    const requiredFields = ['username', 'email', 'password', 'confirmPassword'];

    requiredFields.forEach(field => {
      if (isBlank(form[field]) && fieldSelectedAtLeastOnce[field + "B"]) {
        newErrors[field] = "Potrebno ispuniti";
      }
    });

    if (form.username.length > settings.maxUsernameLength) {
      newErrors.username = "Maksimalna duljina korisničkog imena je " + settings.maxUsernameLength;
    } else if (form.username.length < settings.minUsernameLength && form.username.length !== 0) {
      newErrors.username = "Minimalna duljina korisničkog imena je " + settings.minUsernameLength;
    }

    if (form.email.length > settings.maxEmailLength && !isBlank(form.email)) {
      newErrors.email = "Maksimalna duljina e-mail adrese je " + settings.maxEmailLength;
    }

    if (!settings.emailRegex.test(form.email.toLowerCase()) && !isBlank(form.email)) {
      newErrors.email = "Nije valjana e-mail adresa";
    }

    if (form.password.length > settings.maxPasswordLength && fieldSelectedAtLeastOnce.passwordB && !isBlank(form.password)) {
      newErrors.password = "Maksimalna duljina zaporke je " + settings.maxPasswordLength;
    }

    if (form.password.length < settings.minPasswordLength && fieldSelectedAtLeastOnce.passwordB && !isBlank(form.password)) {
      newErrors.password = "Minimalna duljina zaporke je " + settings.minPasswordLength;
    }

    if (form.password !== form.confirmPassword && fieldSelectedAtLeastOnce.confirmPasswordB) {
      newErrors.confirmPassword = "Zaporke se ne podudaraju";
    }

    return newErrors;
  }

  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  const handleChange = prop => event => {
    setForm({...form, [prop]: event.target.value})
  };

  const handleFocus = prop => {
    setSelectedField({...fieldSelectedAtLeastOnce, [prop + "B"]: true})
  };

  function isValid() {
    return Object.keys(getErrors()).length === 0 && !isBlank(form.username) && !isBlank(form.email)
      && !isBlank(form.password) && !isBlank(form.confirmPassword);
  }

  return (
    <AuthenticationPageWrapper title="Aktiviraj korisnički račun" avatarIcon={<LockIcon/>}>
      <div hidden={!failedRegister} className={classes.alert}>
        <Alert severity="error">
          <Typography variant="subtitle2">Zauzeto korisničko ime ili e-mail adresa</Typography>
        </Alert>
      </div>

      <form className={classes.form} onSubmit={onSubmit}>
        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required >
          <InputLabel htmlFor="username" error={!!errors.username}>Korisničko ime</InputLabel>
          <OutlinedInput
            autoFocus
            id="username"
            type="text"
            autoComplete="username"
            error={!!errors.username}
            value={form.username}
            onChange={handleChange('username')}
            onFocus={() => handleFocus('username')}
            labelWidth={120}
            inputProps={{maxLength: settings.maxUsernameLength}}
          />
          <FormHelperText error={!!errors.username}>{errors.username && errors.username}</FormHelperText>
        </FormControl>

        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required>
          <InputLabel htmlFor="email" error={!!errors.email}>E-mail adresa</InputLabel>
          <OutlinedInput
            id="email"
            type="email"
            autoComplete="email"
            error={!!errors.email}
            value={form.email}
            onChange={handleChange('email')}
            onFocus={() => handleFocus('email')}
            labelWidth={120}
            inputProps={{maxLength: settings.maxEmailLength}}
          />
          <FormHelperText error={!!errors.email}>{errors.email && errors.email}</FormHelperText>
        </FormControl>

        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required>
          <InputLabel htmlFor="password" error={!!errors.password}>Zaporka</InputLabel>
          <OutlinedInput
            id="password"
            type={showPassword ? 'text' : 'password'}
            autoComplete="current-password"
            error={!!errors.password}
            value={form.password}
            onChange={handleChange('password')}
            onFocus={() => handleFocus('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword(!showPassword)}
                  edge="end"
                >
                  {showPassword ? <Visibility/> : <VisibilityOff/>}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
            inputProps={{maxLength: settings.maxPasswordLength}}
          />
          <FormHelperText error={!!errors.password}>{errors.password && errors.password}</FormHelperText>
        </FormControl>

        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required>
          <InputLabel htmlFor="confirmPassword" error={!!errors.confirmPassword}>Potvrdi zaporku</InputLabel>
          <OutlinedInput
            id="confirmPassword"
            type={showPassword ? 'text' : 'password'}
            autoComplete="current-password"
            error={!!errors.confirmPassword}
            value={form.confirmPassword}
            onChange={handleChange('confirmPassword')}
            onFocus={() => handleFocus('confirmPassword')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword(!showPassword)}
                  edge="end"
                >
                  {showPassword ? <Visibility/> : <VisibilityOff/>}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={125}
            inputProps={{maxLength: settings.maxPasswordLength}}
          />
          <FormHelperText
            error={!!errors.confirmPassword}>{errors.confirmPassword && errors.confirmPassword}</FormHelperText>
        </FormControl>

        <Button
          disabled={!isValid()}
          type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
          Aktiviraj korisnički račun
        </Button>

        <Grid container>
          <Grid item xs>
            <Link to="/login">
              <Typography variant="subtitle2" className={classes.color}>
                Već imate korisnički račun? Prijavite se
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </form>

    </AuthenticationPageWrapper>
  );
}

ActivateAccount.propTypes = {
  onLogin: PropTypes.func.isRequired
}