import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import HomeIcon from '@material-ui/icons/Home';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  border: {
    borderStyle: 'solid',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  homeIcon: {
    position: 'absolute',
    marginLeft: theme.spacing(-3),
    color: theme.name === 'default-theme' ? blue[800] : theme.palette.primary.contrastText
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  }
}));

export default function AuthenticationPageWrapper({children, title, avatarIcon}) {

  const classes = useStyles();
  const theme = useTheme();

  const getTheme = () => {
    return theme.name === 'default-theme' ? 'bg-blend-in-with-footer' : 'bg-black';
  }

  return (
    <Grid container component="main" className={`${classes.root} ${getTheme()}`}>
      <CssBaseline/>

      <Container maxWidth="sm" component={Paper} elevation={10} className={classes.border}>
        <Link to="/" className="text-decoration-none">
          <IconButton aria-label="close" color="primary" className={classes.homeIcon} title="Povratak na početnu">
            <HomeIcon/>
          </IconButton>
        </Link>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            {avatarIcon}
          </Avatar>
          <Typography component="h1" variant="h5">
            {title}
          </Typography>

          {children}

        </div>
      </Container>
    </Grid>
  );
}

AuthenticationPageWrapper.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  avatarIcon: PropTypes.element.isRequired
};