import React from 'react';
import PropTypes from 'prop-types';
import {Link, useHistory} from 'react-router-dom';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Alert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import * as Constants from '../../Constants';
import AuthenticationPageWrapper from './AuthenticationPageWrapper';

const useStyles = makeStyles(theme => ({
  alert: {
    width: '100%',
    marginTop: theme.spacing(2)
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  color: {
    color: theme.name === 'default-theme' ? 'default' : theme.palette.primary.contrastText
  }
}));

export default function Login({onLogin}) {

  const history = useHistory();
  const classes = useStyles();

  const [form, setForm] = React.useState({
    username: '',
    password: '',
  });
  const [failedLogin, setFailedLogin] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  function onSubmit(e) {
    e.preventDefault();

    const data = new URLSearchParams();
    data.append('username', form.username);
    data.append('password', form.password);

    const loginData = {
      method: 'POST',
      credentials: 'include',
      body: data
    };

    fetch(Constants.LOGIN, loginData)
      .then(response => {
        if (response.url.endsWith("?error")) {
          setFailedLogin(true);
          setForm({username: '', password: ''});
        } else {
          history.push('/');
          onLogin();
        }
      }).catch(() => console.warn("Login Error"));
  }

  const handleChange = prop => event => {
    setForm({...form, [prop]: event.target.value})
  };

  function isValid() {
    return !isBlank(form.username) && !isBlank(form.password);
  }

  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  return (
    <AuthenticationPageWrapper title="Prijava" avatarIcon={<LockOutlinedIcon/>}>
      <div hidden={!failedLogin} className={classes.alert}>
        <Alert severity="error">
          <Typography variant="subtitle2">Neispravno korisničko ime ili zaporka</Typography>
        </Alert>
      </div>

      <form className={classes.form} onSubmit={onSubmit}>
        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required>
          <InputLabel htmlFor="username">Korisničko ime</InputLabel>
          <OutlinedInput
            autoFocus
            id="username"
            type="text"
            autoComplete="username"
            value={form.username}
            onChange={handleChange('username')}
            labelWidth={120}
          />
        </FormControl>

        <FormControl fullWidth className={clsx(classes.margin)} margin="normal"
                     variant="outlined" required>
          <InputLabel htmlFor="password">Zaporka</InputLabel>
          <OutlinedInput
            id="password"
            autoComplete="current-password"
            type={showPassword ? 'text' : 'password'}
            value={form.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShowPassword(!showPassword)}
                  // onMouseEnter={() => setShowPassword(true)}
                  // onMouseLeave={() => setShowPassword(false)}
                  edge="end"
                >
                  {showPassword ? <Visibility/> : <VisibilityOff/>}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>

        <Button type="submit" fullWidth disabled={!isValid()} variant="contained" color="primary"
                className={classes.submit}>
          Prijavi se
        </Button>

        <Grid container>
          <Grid item xs>
            <Link to="/activate-account">
              <Typography variant="subtitle2" className={classes.color}>
                Aktivirajte korisnički račun
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </form>
    </AuthenticationPageWrapper>
  );
}

Login.propTypes = {
  onLogin: PropTypes.func.isRequired
}
