import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  footer: {
    textAlign: 'center',
    padding: theme.spacing(3, 2),
    display: 'flex',
    zIndex: 0,
    position: 'relative'
  }
}));

export default function Footer() {

  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Container maxWidth="sm">
        <Typography color="textSecondary">
          Copyright @ Paulo Sanković @ FER 2020
        </Typography>
      </Container>
    </footer>
  );
}