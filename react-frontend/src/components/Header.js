import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import Logo from '../questionAnswerWhite.png';
import Container from '@material-ui/core/Container';
import AppBarMenu from './AppBarMenu';
import Accessibility from './accessibility/Accessibility';
import SidebarMenu from './SidebarMenu';
import {GroupContext} from '../App';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    alignItems: 'flex-start',
    display: 'inline-grid',
    padding: '20px 40px'
  },
  buttonBorder: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  },
  title: {
    flexGrow: 1,
    alignSelf: 'center'
  },
  centerGrid: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'end'
  },
  background: {
    background: theme.palette.background.pinotNoir
  }
}));

export default function Header({title, subtitle, categoryName, height, randomBg}) {

  const classes = useStyles();
  const currentUser = React.useContext(GroupContext).currentUser;

  const backgrounds = [
    "bg-pinot-noir",
    "bg-visions-of-grandeur",
    "bg-witching-hour",
    "bg-man-of-steel",
    "bg-terminal",
    "bg-christmas",
    "bg-royal-blue-petrol",
    "bg-slight-ocean-view",
    "bg-magic-ray"
  ];

  return (
    <React.Fragment>
      <AppBar position="relative" className={classes.background} style={{zIndex: 0}}>
        <Toolbar className={classes.toolbar} style={{minHeight: `${height}vh`}}>
          <Grid container justify="space-between" alignItems="center" className={classes.topbar}>
            <div>
              {currentUser && <SidebarMenu/>}
            </div>
            <div style={{display: 'inline-flex', position: 'end'}}>
              {currentUser && <AppBarMenu/>}
              {!currentUser && (
                <Link to="/login" className="text-decoration-none">
                  <Button color="primary" variant="contained" className={classes.buttonBorder}>
                    Prijavi se
                  </Button>
                </Link>
              )}
              <Accessibility/>
            </div>
          </Grid>

          <Container maxWidth="lg">
            <Grid container>
              <Grid item className={classes.title} xs={12} md={6}>
                <Typography variant="h4">{title}</Typography>
                {categoryName && <Typography variant="h6">{categoryName}</Typography>}
                {subtitle && <Typography variant="button">{subtitle}</Typography>}
              </Grid>
              <Hidden smDown>
                <Grid item xs={12} md={6}>
                  <Link to="/" className={classes.centerGrid}>
                    <img src={Logo} alt="Imidž obrazovnih institucija logo" style={{width: '70%'}}/>
                  </Link>
                </Grid>
              </Hidden>
            </Grid>
          </Container>

        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  categoryName: PropTypes.string,
  height: PropTypes.number.isRequired,
  randomBg: PropTypes.bool
}
