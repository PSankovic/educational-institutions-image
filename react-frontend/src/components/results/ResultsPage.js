import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Header from '../Header';
import PageContentWrapper from '../PageContentWrapper';
import QuestionResultCard from './QuestionResultCard';

const useStyles = makeStyles((theme) => ({
  container: {
    [theme.breakpoints.only('xs')]: {
      padding: 0
    }
  },
  border: {
    borderStyle: 'dotted',
    border: theme.name !== 'default-theme' ? 1 : 0,
    borderRadius: 4
  }
}));

export default function ResultsPage({sector}) {

  const classes = useStyles();
  const theme = useTheme();

  const [selectedCategory, setSelectedCategory] = React.useState(
    sector.categories.length > 0 && sector.categories[0].name);

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const sortQuestions = (questions) => {
    return questions.sort(function (a, b) {
      return a.index - b.index || a.text.localeCompare(b.text);
    });
  }

  function getCategory(category) {
    if (category.name === selectedCategory) {
      return (
        <Grid key={category.id} container spacing={2}>
          {category.questions && sortQuestions(category.questions).map((question) =>
            <QuestionResultCard key={question.id} question={question}/>
          )}
        </Grid>
      )
    }
  }

  const getColor = (n) => {
    if (n === 1) {
      if (theme.name === 'default-theme') {
        return selectedCategory !== 'Student' ? 'primary' : 'default';
      } else {
        if (selectedCategory === '') {
          return 'primary';
        }
        return selectedCategory !== 'Student' ? 'default' : 'primary';
      }
    } else {
      if (theme.name === 'default-theme') {
        return selectedCategory !== 'Poslodavac' ? 'secondary' : 'default';
      } else {
        if (selectedCategory === '') {
          return 'secondary';
        }
        return selectedCategory !== 'Poslodavac' ? 'default' : 'secondary';
      }
    }
  };

  return (
    <React.Fragment>
      <Header title={sector.name} height={40}/>

      <PageContentWrapper>
        <Container maxWidth="md" className={classes.container}>

          {sector.description &&
          <Grid item xs={12}>
            <Card raised={true}>
              <Box border={1} borderRadius={6} className={classes.border}>
                <Box className="bg-instagram" style={{height: '10px'}}/>
                <CardContent>
                  <Typography align="center">
                    <em>{sector.description}</em>
                  </Typography>
                </CardContent>
              </Box>
            </Card>
          </Grid>}

          <Box display="flex" justifyContent="center" my={2}>
            <Box mr={1}>
              <Button color={getColor(1)} variant="contained"
                      onClick={() => setSelectedCategory('Poslodavac')} className={classes.border}>
                Poslodavac
              </Button>
            </Box>

            <Box ml={1}>
              <Button color={getColor(2)} variant="contained"
                      onClick={() => setSelectedCategory('Student')} className={classes.border}>
                Student
              </Button>
            </Box>
          </Box>

          {sector.categories && sector.categories.map((category) => getCategory(category))}
        </Container>
      </PageContentWrapper>
    </React.Fragment>
  );
}

ResultsPage.propTypes = {
  sector: PropTypes.object.isRequired
}