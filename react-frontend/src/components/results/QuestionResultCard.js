import React from 'react';
import PropTypes from 'prop-types';
import CanvasJSReact from '../../assets/canvasjs.react';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import useTheme from '@material-ui/core/styles/useTheme';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default function QuestionResultCard({question}) {

  const theme = useTheme();

  const getTotalVotesCount = () => {
    let total = 0;
    question.answers.forEach((answer) => total += answer.voteCount);
    return total;
  }

  const generateData = () => {
    const total = getTotalVotesCount();
    const data = [];
    question.answers.map((answer) =>
      data.push({label: answer.text, y: answer.voteCount, percent: Math.round(answer.voteCount / total * 100)})
    );
    return data;
  }

  function getTitle() {
    if (window.innerWidth < theme.breakpoints.width("md")) {
      return {};
    }
    return {
      text: `${question.index}. ${question.text}`,
      fontSize: 20,
      padding: 20,
      fontColor: theme.name === 'high-contrast-theme' ? "#feff26" : "black"
    };
  }

  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: theme.name === 'high-contrast-theme' ? "dark2" : "light1",
    title: getTitle(),
    subtitles: [{
      text: `Ukupni broj glasova: ${getTotalVotesCount()}`,
      fontSize: 17,
      padding: 10,
      verticalAlign: 'bottom',
      fontWeight: 'normal',
      fontColor: theme.name === 'high-contrast-theme' ? "#feff26" : "black"
    }],
    data: [{
      type: 'pie',
      indexLabel: "{label}: {percent}%",
      dataPoints: generateData()
    }]
  }

  return (
    <Grid item xs={12}>
      <Card raised={true}>
        <CardContent>
          <Hidden mdUp>
            <Box fontWeight="fontWeightBold" display="flex" alignItems="center" pt={1} pb={2} px={1}>
              <Box mr={1}>
                <Typography><b>{question.index}.</b></Typography>
              </Box>
              <Box mr={1}>
                <Typography><b>{question.text}</b></Typography>
              </Box>
              {question.required && <Typography style={{color: red[500]}}><b>*</b></Typography>}
            </Box>
          </Hidden>
          <Box p={1}>
            <CanvasJSChart options={options}/>
          </Box>
        </CardContent>
      </Card>
    </Grid>
  );
}

QuestionResultCard.propTypes = {
  question: PropTypes.object.isRequired
}