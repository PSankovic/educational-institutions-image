// const API_URL = 'http://localhost:8080';
const API_URL = 'https://instedurep-back-end.herokuapp.com';

// App.js
export const GET_ACTIVE_SECTORS = API_URL + '/sectors/active';
export const GET_CURRENT_USER = API_URL + '/user/current';
export const LOGOUT = API_URL + '/logout';

// Login.js & ActivateAccount.js
export const LOGIN = API_URL + '/login';

// ActivateAccount.js
export const REGISTER = API_URL + '/user/register';

// QueryPage.js
export const VOTE = API_URL + '/answer';
export const POST_TEXT_ANSWER = API_URL + '/question/post-text-answer';

// UsersPage.js
export const GET_NON_ACTIVATED_EMAILS = API_URL + '/activation-email/get-non-activated';
export const GET_ALL_USERS = API_URL + '/user/get-all';
export const PROMOTE_USER = API_URL + '/user/promote';
export const DEMOTE_USER = API_URL + '/user/demote';
export const DEACTIVATE_USER = API_URL + '/user/deactivate';
export const REACTIVATE_USER = API_URL + '/user/reactivate';
export const REGISTER_EMAIL = API_URL + '/activation-email/register';
export const DELETE_EMAIL = API_URL + '/activation-email'

// EditSectorsPage.js
export const ADD_SECTOR = API_URL + '/sectors/add-sector';
export const DEACTIVATE_SECTOR = API_URL + '';

// SectorCard.js
export const UPDATE_SECTOR_NAME_DESCRIPTION = API_URL + '/sectors/update-name-description';

// EditQueryPage.js
export const UPDATE_QUESTION = API_URL + '/question/update';
export const GET_QUESTIONS = API_URL + '/category/questions';
export const POST_QUESTIONS = API_URL + '/category/add-questions';

// UniversityPicker.js
export const GET_UNIVERSITIES = API_URL + '/universities';