import {createMuiTheme} from "@material-ui/core/styles";

export const defaultTheme = createMuiTheme( {
  name: 'default-theme',
  palette: {
    background: {
      pinotNoir: 'linear-gradient(to bottom, #000428, #004e92)',
      sidebar: 'linear-gradient(to bottom, #f8f9fc, #9ba1ff)'
    },
    border: '#fff'
  },
});