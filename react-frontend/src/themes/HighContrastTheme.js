import {createMuiTheme} from "@material-ui/core/styles";

export const highContrastTheme = createMuiTheme({
  name: 'high-contrast-theme',
  palette: {
    primary: {
      main: '#000',
      contrastText: '#feff26',
    },
    secondary: {
      main: '#000',
      contrastText: '#feff26',
    },
    error: {
      main: '#000',
      contrastText: '#feff26'
    },
    warning: {
      main: '#000',
      contrastText: '#feff26'
    },
    info: {
      main: '#000',
      contrastText: '#feff26'
    },
    success: {
      main: '#000',
      contrastText: '#feff26'
    },
    grey: {
      300: '#6b6b10'
    },
    text: {
      primary: '#feff26',
      secondary: '#feff26',
      textPrimary: '#feff26',
      textSecondary: '#feff26',
      textInfo: '#feff26',
      textDefault: '#feff26'
    },
    divider: '#feff26',
    background: {
      paper: '#000',
      default: '#000',
      pinotNoir: '#000',
      sidebar: '#000'
    },
    border: '#feff26'
  },
  typography: {
    fontSize: 20
  }
});