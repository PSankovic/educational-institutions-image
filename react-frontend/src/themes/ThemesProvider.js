import React from 'react';
import PropTypes from 'prop-types';
import {ThemeProvider} from "@material-ui/core/styles";
import {defaultTheme} from './DefaultTheme';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export const ThemeContext = React.createContext([]);

export default function ThemesProvider({children}) {

  const [theme, setTheme] = React.useState(defaultTheme);
  const [fontSize, setFontSize] = React.useState(defaultTheme.typography.fontSize);

  function increaseFontSize() {
    const size = fontSize * 1.1;
    console.log(size);
    if (size < 20) {
      setFontSize(size);
    }
  }

  function decreaseFontSize() {
    const size = fontSize / 1.1;
    console.log(size);
    if (size > 10) {
      setFontSize(size);
    }
  }

  function resetFontSize() {
    setFontSize(defaultTheme.typography.fontSize);
  }

  return (
    <ThemeContext.Provider value={{setTheme, increaseFontSize, decreaseFontSize, resetFontSize}}>
      <ThemeProvider theme={theme}>
        <ThemeProvider theme={(theme) =>
          createMuiTheme({
            ...theme,
            typography: {
              fontSize: fontSize
            }
          })}>

          {children}

        </ThemeProvider>
      </ThemeProvider>
    </ThemeContext.Provider>
  );
}

ThemesProvider.propTypes = {
  children: PropTypes.node.isRequired
}