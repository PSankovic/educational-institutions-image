package com.fer.ztl.edu_institutions_image;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduInstitutionsImageApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduInstitutionsImageApplication.class, args);
    }

}
