package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.ActivationEmail;
import com.fer.ztl.edu_institutions_image.services.ActivationEmailService;
import com.fer.ztl.edu_institutions_image.web.dto.ActivationEmailDto;
import com.fer.ztl.edu_institutions_image.web.exceptions.ActivationEmailAlreadyExistsException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/activation-email")
public class ActivationEmailController {
    
    private final Logger logger = LoggerFactory.getLogger(QuestionController.class);
    
    private final ModelMapper modelMapper;
    
    private final ActivationEmailService activationEmailService;
    
    public ActivationEmailController(ModelMapper modelMapper, ActivationEmailService activationEmailService) {
        this.modelMapper = modelMapper;
        this.activationEmailService = activationEmailService;
    }
    
    @PostMapping("/register")
    @Secured({ "ROLE_ADMIN" })
    public void registerEmail(@RequestParam String email) {
        if (activationEmailService.getByEmail(email) != null) {
            logger.info("Activation email " + email + " already exists!");
            throw new ActivationEmailAlreadyExistsException();
        }
        activationEmailService.save(new ActivationEmail(email));
    }
    
    @GetMapping("/get-non-activated")
    @Secured({ "ROLE_ADMIN" })
    public List<ActivationEmailDto> getNonActivatedEmails() {
        return activationEmailService.getAllNonActivated().stream()
                .map(email -> modelMapper.map(email, ActivationEmailDto.class))
                .collect(Collectors.toList());
    }
    
    @GetMapping("/get-all")
    @Secured({ "ROLE_ADMIN" })
    public List<ActivationEmailDto> getAll() {
        return activationEmailService.getAll().stream()
                .map(email -> modelMapper.map(email, ActivationEmailDto.class))
                .collect(Collectors.toList());
    }
    
    @DeleteMapping
    @Secured({ "ROLE_ADMIN" })
    public void deactivateActivationEmail(@RequestParam @Email String email) {
        activationEmailService.deleteEmail(email);
    }
}
