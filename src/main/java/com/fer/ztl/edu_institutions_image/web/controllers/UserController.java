package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.User;
import com.fer.ztl.edu_institutions_image.model.UserRoleEnum;
import com.fer.ztl.edu_institutions_image.services.ActivationEmailService;
import com.fer.ztl.edu_institutions_image.services.UserService;
import com.fer.ztl.edu_institutions_image.web.dto.UserDto;
import com.fer.ztl.edu_institutions_image.web.dto.UserRegistrationDto;
import com.fer.ztl.edu_institutions_image.web.exceptions.InvalidEmailActivationRequestException;
import com.fer.ztl.edu_institutions_image.web.exceptions.InvalidUserRoleChangeException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {
    
    private final Logger logger = LoggerFactory.getLogger(QuestionController.class);
    
    private final ModelMapper modelMapper;
    
    private final UserService userService;
    
    private final ActivationEmailService activationEmailService;
    
    public UserController(ModelMapper modelMapper, UserService userService, ActivationEmailService activationEmailService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.activationEmailService = activationEmailService;
    }
    
    @GetMapping("/current")
    public UserDto getCurrentUser() {
        User user = userService.getCurrentUser();
        return modelMapper.map(user, UserDto.class);
    }
    
    @PostMapping("/register")
    public void register(@RequestBody UserRegistrationDto userDto) {
        if (!activationEmailService.isPresentAndNotActivated(userDto.getEmail())) {
            throw new InvalidEmailActivationRequestException();
        }
        userDto.addRole(UserRoleEnum.MODERATOR);
        User user = modelMapper.map(userDto, User.class);
        userService.save(user);
        activationEmailService.activateEmail(userDto.getEmail());
        logger.info("User " + user.getUsername() + " successfully registered!");
    }
    
    @PutMapping("/promote")
    @Secured({"ROLE_ADMIN"})
    public void promoteUser(@RequestParam String username) {
        User user = userService.getByUsername(username);
        if (user.getRoles().contains(UserRoleEnum.ADMIN)) {
            throw new InvalidUserRoleChangeException();
        }
        user.setRoles(new HashSet<>(List.of(UserRoleEnum.ADMIN)));
        userService.save(user);
    }
    
    @PutMapping("/demote")
    @Secured({"ROLE_ADMIN"})
    public void demoteUser(@RequestParam String username) {
        User user = userService.getByUsername(username);
        if (user.getRoles().contains(UserRoleEnum.MODERATOR)) {
            throw new InvalidUserRoleChangeException();
        }
        user.setRoles(new HashSet<>(List.of(UserRoleEnum.MODERATOR)));
        userService.save(user);
    }
    
    @GetMapping("/get-all")
    @Secured({"ROLE_ADMIN"})
    public List<UserDto> getAll() {
        return userService.getAll().stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect(Collectors.toList());
    }
    
    @PutMapping("/reactivate")
    @Secured({"ROLE_ADMIN"})
    public void reactivateUser(@RequestParam String username) {
        userService.activateUser(username);
    }
    
    @DeleteMapping("/deactivate")
    @Secured({"ROLE_ADMIN"})
    public void deactivateUser(@RequestParam String username) {
        userService.deleteByUsername(username);
    }
}
