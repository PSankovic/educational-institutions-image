package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.Sector;
import com.fer.ztl.edu_institutions_image.services.SectorService;
import com.fer.ztl.edu_institutions_image.web.dto.SectorDto;
import com.fer.ztl.edu_institutions_image.web.exceptions.SectorDeletedException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/sectors")
public class SectorController {
    
    private final Logger logger = LoggerFactory.getLogger(QuestionController.class);
    
    private final ModelMapper modelMapper;
    
    private final SectorService sectorService;
    
    public SectorController(ModelMapper modelMapper, SectorService sectorService) {
        this.modelMapper = modelMapper;
        this.sectorService = sectorService;
    }
    
    @GetMapping("/active")
    public List<SectorDto> getActiveSectors() {
        List<Sector> sectors = sectorService.getActiveSectorsSorted();
        return sectors.stream()
                .map(sector -> modelMapper.map(sector, SectorDto.class))
                .collect(Collectors.toList());
    }
    
    @GetMapping("/get-sector")
    public SectorDto getSector(@RequestParam String name) {
        Sector sector = sectorService.getByName(name);
        if (sector.getDeleted()) {
            throw new SectorDeletedException();
        }
        return modelMapper.map(sector, SectorDto.class);
    }
    
    @GetMapping("/get-all")
    public List<SectorDto> getAllSectors() {
        return sectorService.getAllSorted().stream()
                .map(sector -> modelMapper.map(sector, SectorDto.class))
                .collect(Collectors.toList());
    }
    
    @PostMapping("/add-sector")
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void addSector(@RequestParam String name) {
        Sector sector = new Sector();
        sector.setName(name);
        sectorService.save(sector);
    }
    
    @PutMapping("/update-name-description")
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void updateNameAndDescription(@RequestParam Long id, @RequestParam String name,
                                         @RequestParam String description) {
        sectorService.updateNameAndDescription(id, name, description);
    }
    
    @DeleteMapping
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void deleteSector(@RequestParam String name) {
        sectorService.deleteByName(name);
    }
}
