package com.fer.ztl.edu_institutions_image.web.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ActivationEmailDto {
    
    @Email
    @Size(max = 256)
    private String email;
    
    @NotNull
    private Boolean activated = false;
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public Boolean getActivated() {
        return activated;
    }
    
    public void setActivated(Boolean activated) {
        this.activated = activated;
    }
}
