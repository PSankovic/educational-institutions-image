package com.fer.ztl.edu_institutions_image.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

public class TextAnswerDto {
    
    @Positive
    private Long questionId;
    
    @NotEmpty
    private String text;
    
    public Long getQuestionId() {
        return questionId;
    }
    
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
}
