package com.fer.ztl.edu_institutions_image.web.dto;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public class UpdateQuestionDto {
    
    @NotEmpty
    private QuestionDto question;
    
    private List<TextIndexAnswerDto> updatedAnswers;
    
    public QuestionDto getQuestion() {
        return question;
    }
    
    public void setQuestion(QuestionDto question) {
        this.question = question;
    }
    
    public List<TextIndexAnswerDto> getUpdatedAnswers() {
        return updatedAnswers;
    }
    
    public void setUpdatedAnswers(List<TextIndexAnswerDto> updatedAnswers) {
        this.updatedAnswers = updatedAnswers;
    }
}
