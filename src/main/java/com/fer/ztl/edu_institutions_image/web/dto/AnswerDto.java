package com.fer.ztl.edu_institutions_image.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class AnswerDto {
    
    private Long id;
    
    @NotEmpty
    @Size(max = 256)
    private String text;
    
    private Long index;
    
    @PositiveOrZero
    private Long voteCount = 0L;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public Long getIndex() {
        return index;
    }
    
    public void setIndex(Long index) {
        this.index = index;
    }
    
    public Long getVoteCount() {
        return voteCount;
    }
    
    public void setVoteCount(Long voteCount) {
        this.voteCount = voteCount;
    }
}
