package com.fer.ztl.edu_institutions_image.web.dto;

import com.fer.ztl.edu_institutions_image.model.Question;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

public class CategoryDto {
    
    private Long id;
    
    @NotEmpty
    @Size(max = 128)
    private String name;
    
    private List<Question> questions;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public List<Question> getQuestions() {
        return questions;
    }
    
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
