package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.Answer;
import com.fer.ztl.edu_institutions_image.services.AnswerService;
import com.fer.ztl.edu_institutions_image.web.dto.AnswerDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/answer")
public class AnswerController {
    
    private final Logger logger = LoggerFactory.getLogger(AnswerController.class);
    
    private final ModelMapper modelMapper;
    
    private final AnswerService answerService;
    
    public AnswerController(ModelMapper modelMapper, AnswerService answerService) {
        this.modelMapper = modelMapper;
        this.answerService = answerService;
    }
    
    @PostMapping
    public AnswerDto createAnswer(@RequestParam String text, Long index) {
        Answer answer = new Answer(text, index);
        return modelMapper.map(answerService.save(answer), AnswerDto.class);
    }
    
    @PutMapping
    public List<AnswerDto> vote(@RequestBody List<Long> ids) {
        List<Answer> answers = new ArrayList<>();
        for (Long id : ids) {
            answers.add(answerService.increaseVoteCounter(id));
        }
        return answers.stream().map(answer -> modelMapper.map(answer, AnswerDto.class)).collect(Collectors.toList());
    }
    
    @DeleteMapping
    public void deleteAnswer(@RequestParam Long id) {
        answerService.deleteById(id);
    }
}
