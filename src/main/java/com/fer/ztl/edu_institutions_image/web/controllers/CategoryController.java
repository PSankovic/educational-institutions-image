package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.Answer;
import com.fer.ztl.edu_institutions_image.model.Category;
import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.services.CategoryService;
import com.fer.ztl.edu_institutions_image.services.QuestionService;
import com.fer.ztl.edu_institutions_image.web.dto.QuestionDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/category")
public class CategoryController {
    
    private final Logger logger = LoggerFactory.getLogger(QuestionController.class);
    
    private final ModelMapper modelMapper;
    
    private final QuestionService questionService;
    private final CategoryService categoryService;
    
    public CategoryController(ModelMapper modelMapper, QuestionService questionService, CategoryService categoryService) {
        this.modelMapper = modelMapper;
        this.questionService = questionService;
        this.categoryService = categoryService;
    }
    
    @GetMapping("/questions")
    public List<QuestionDto> getQuestionsForCategory(@RequestParam Long id) {
        List<Question> questions = categoryService.getQuestionsForId(id);
        return questions.stream()
                .map(question -> modelMapper.map(question, QuestionDto.class))
                .collect(Collectors.toList());
    }
    
    @PostMapping("/add-questions")
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void addQuestions(@RequestParam Long id, @RequestBody List<QuestionDto> questionsDto) {
        Category category = categoryService.getById(id);
        List<Question> questions = questionsDto.stream()
                .map(questionDto -> modelMapper.map(questionDto, Question.class))
                .collect(Collectors.toList());
        
        for (Question question : questions) {
            List<Answer> answers = question.getAnswers()
                    .stream().map(answer -> new Answer(answer.getText(), answer.getIndex()))
                    .collect(Collectors.toList());
            
            Question newQuestion = questionService.save(
                    new Question(question.getText(), question.getIndex(), answers, question.getQuestionType(),
                            question.getRequired()));
            
            category.addQuestion(newQuestion);
        }
        categoryService.save(category);
    }
}
