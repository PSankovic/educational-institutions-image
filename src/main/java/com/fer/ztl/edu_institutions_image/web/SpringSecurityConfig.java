package com.fer.ztl.edu_institutions_image.web;

import com.fer.ztl.edu_institutions_image.services.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    
    private final UserService userService;
    
    public SpringSecurityConfig(UserService userService) {
        this.userService = userService;
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                
                .and()
                    .csrf()
                    .disable()
                
                .authorizeRequests()
                    .antMatchers("/h2-console/**", "/sector/get-all", "/user/register", "/sectors/active",
                            "/answer", "/question/update", "/question/post-text-answer",
                            "/universities")
                    .permitAll()
                    .anyRequest()
                    .authenticated()
                
                .and()
                    .formLogin()
                    .permitAll()
                
                .and()
                    .logout()
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .permitAll();
        
        http.headers().frameOptions().disable();
    }
    
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(
                Collections.unmodifiableList(List.of("https://instedurep.herokuapp.com", "http://localhost:3000",
                        "https://instedurep.azurewebsites.net")));
        configuration.setAllowedMethods(Collections.unmodifiableList(List.of("GET", "POST", "PUT", "DELETE")));
        configuration.setAllowedHeaders(Collections.unmodifiableList(List.of("Authorization", "Cache-Control", "Content-Type")));
        
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(getPasswordEncoder());
    }
    
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
