package com.fer.ztl.edu_institutions_image.web.controllers.util;

import com.fer.ztl.edu_institutions_image.model.Answer;
import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.model.User;
import com.fer.ztl.edu_institutions_image.web.dto.AnswerDto;
import com.fer.ztl.edu_institutions_image.web.dto.QuestionDto;
import com.fer.ztl.edu_institutions_image.web.dto.UserRegistrationDto;
import com.fer.ztl.edu_institutions_image.web.exceptions.PasswordConfirmationMismatchException;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class ModelMapperConfiguration {
    
    private final PasswordEncoder passwordEncoder;
    
    public ModelMapperConfiguration(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    @Bean
    public ModelMapper modelMapper() {
        Logger logger = LoggerFactory.getLogger(ModelMapper.class);
        
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        
        Converter<UserRegistrationDto, User> userRegistrationDtoUserConverter = context -> {
            UserRegistrationDto registrationDto = context.getSource();
            if (registrationDto == null) {
                return null;
            }
            if (!registrationDto.getPassword().equals(registrationDto.getConfirmPassword())) {
                logger.info("Confirmation password mismatch during user registration!");
                throw new PasswordConfirmationMismatchException();
            }
            
            return new User(registrationDto.getUsername(), registrationDto.getEmail(),
                    passwordEncoder.encode(registrationDto.getPassword()), registrationDto.getRoles());
        };
        modelMapper.typeMap(UserRegistrationDto.class, User.class).setConverter(userRegistrationDtoUserConverter);
        
        Converter<Question, QuestionDto> questionDtoConverter = context -> {
            Question question = context.getSource();
            
            if (question == null) {
                return null;
            }
            
            QuestionDto questionDto = new QuestionDto();
            questionDto.setId(question.getId());
            questionDto.setText(question.getText());
            questionDto.setIndex(question.getIndex());
            questionDto.setQuestionType(question.getQuestionType());
            questionDto.setRequired(question.getRequired());
            
            List<AnswerDto> answerDto = question.getAnswers().stream()
                    .map(answer -> modelMapper.map(answer, AnswerDto.class))
                    .collect(Collectors.toList());
            questionDto.setAnswers(answerDto);
            
            return questionDto;
        };
        modelMapper.typeMap(Question.class, QuestionDto.class).setConverter(questionDtoConverter);
        
        Converter<QuestionDto, Question> dtoQuestionConverter = context -> {
            QuestionDto questionDto = context.getSource();
            
            if (questionDto == null) {
                return null;
            }
            
            Question question = new Question();
            question.setId(questionDto.getId());
            question.setText(questionDto.getText());
            question.setIndex(questionDto.getIndex());
            question.setAnswers(questionDto.getAnswers().stream()
                    .map(answer -> modelMapper.map(answer, Answer.class)).collect(Collectors.toList()));
            question.setQuestionType(questionDto.getQuestionType());
            question.setRequired(questionDto.getRequired());
            return question;
        };
        modelMapper.typeMap(QuestionDto.class, Question.class).setConverter(dtoQuestionConverter);
        
        return modelMapper;
    }
    
}
