package com.fer.ztl.edu_institutions_image.web.dto;

import com.fer.ztl.edu_institutions_image.model.UserRoleEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UserRegistrationDto {
    
    @NotEmpty
    @Size(min = 2, max = 32)
    private String username;
    
    @Email
    @NotEmpty
    @Size(max = 256)
    private String email;
    
    @NotEmpty
    @Size(min = 4, max = 256)
    private String password;
    
    @NotEmpty
    @Size(min = 4, max = 256)
    private String confirmPassword;
    
    private Set<UserRoleEnum> roles;
    
    private Boolean deactivated;
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getConfirmPassword() {
        return confirmPassword;
    }
    
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    
    public Set<UserRoleEnum> getRoles() {
        return roles;
    }
    
    public void addRole(UserRoleEnum role) {
        if (roles == null) {
            roles = new HashSet<>();
        }
        roles.add(role);
    }
    
    public void setRoles(Set<UserRoleEnum> roles) {
        this.roles = roles;
    }
    
    public Boolean getDeactivated() {
        return deactivated;
    }
    
    public void setDeactivated(Boolean deactivated) {
        this.deactivated = deactivated;
    }
}
