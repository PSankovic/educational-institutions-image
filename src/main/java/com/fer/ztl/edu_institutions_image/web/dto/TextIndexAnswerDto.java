package com.fer.ztl.edu_institutions_image.web.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;

public class TextIndexAnswerDto {
    
    @NotEmpty
    @Size(max = 256)
    private String text;
    
    private Long index;
    
    public TextIndexAnswerDto(@NotEmpty @Size(max = 256) String text, Long index) {
        this.text = text;
        this.index = index;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public Long getIndex() {
        return index;
    }
    
    public void setIndex(Long index) {
        this.index = index;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TextIndexAnswerDto that = (TextIndexAnswerDto) o;
        return Objects.equals(text, that.text) &&
                Objects.equals(index, that.index);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(text, index);
    }
}
