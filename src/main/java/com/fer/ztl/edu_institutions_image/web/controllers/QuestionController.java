package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.model.Answer;
import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.services.QuestionService;
import com.fer.ztl.edu_institutions_image.web.dto.QuestionDto;
import com.fer.ztl.edu_institutions_image.web.dto.TextAnswerDto;
import com.fer.ztl.edu_institutions_image.web.dto.TextIndexAnswerDto;
import com.fer.ztl.edu_institutions_image.web.dto.UpdateQuestionDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/question")
public class QuestionController {
    
    private final Logger logger = LoggerFactory.getLogger(QuestionController.class);
    
    private final ModelMapper modelMapper;
    
    private final QuestionService questionService;
    
    private final AnswerController answerController;
    
    public QuestionController(ModelMapper modelMapper, QuestionService questionService, AnswerController answerController) {
        this.modelMapper = modelMapper;
        this.questionService = questionService;
        this.answerController = answerController;
    }
    
    @PostMapping("/new")
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void postQuestions(@RequestBody List<QuestionDto> questionsDto) {
        questionsDto.forEach(questionDto -> {
            Question question = modelMapper.map(questionDto, Question.class);
            questionService.save(question);
        });
    }
    
    @PostMapping("/post-text-answer")
    public void postTextAnswer(@RequestBody List<TextAnswerDto> textAnswers) {
        textAnswers.forEach(entry -> {
            Long questionId = entry.getQuestionId();
            String text = entry.getText();
            
            Question question = questionService.getById(questionId);
            List<Answer> answers = question.getAnswers().stream()
                    .filter(a -> a.getText().equals(text)).collect(Collectors.toList());
            
            if (answers.isEmpty()) {
                answers.add(modelMapper.map(answerController.createAnswer(text, 1L), Answer.class));
            }
            
            List<Long> listId = answers.stream().map(Answer::getId).collect(Collectors.toList());
            List<Answer> updatedAnswers = answerController.vote(listId).stream()
                    .map(answerDto -> modelMapper.map(answerDto, Answer.class)).collect(Collectors.toList());
            
            for (Answer answer : updatedAnswers) {
                if (!question.getAnswers().contains(answer)) {
                    question.addAnswer(answer);
                }
            }
            questionService.save(question);
        });
    }
    
    @PutMapping("/update")
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void updateQuestions(@RequestBody List<UpdateQuestionDto> updateQuestionsDto) {
        updateQuestionsDto.forEach(updateQuestionDto -> {
            Question updatedQuestion = modelMapper.map(updateQuestionDto.getQuestion(), Question.class);
            List<TextIndexAnswerDto> originalAnswerTexts = updatedQuestion.getAnswers().stream()
                    .map(answer -> new TextIndexAnswerDto(answer.getText(), answer.getIndex()))
                    .collect(Collectors.toList());
            
            List<TextIndexAnswerDto> newTexts = updateQuestionDto.getUpdatedAnswers().stream()
                    .filter(answer -> !originalAnswerTexts.contains(answer))
                    .collect(Collectors.toList());
            
            updatedQuestion.getAnswers().removeAll(updatedQuestion.getAnswers().stream()
                    .filter(answer -> !updateQuestionDto.getUpdatedAnswers()
                            .contains(new TextIndexAnswerDto(answer.getText(), answer.getIndex())))
                    .collect(Collectors.toList()));
            
            questionService.save(updatedQuestion);
            
            for (TextIndexAnswerDto textIndex : newTexts) {
                updatedQuestion.addAnswer(modelMapper
                        .map(answerController.createAnswer(textIndex.getText(), textIndex.getIndex()), Answer.class));
            }
            questionService.save(updatedQuestion);
        });
    }
    
    @GetMapping
    public Question getQuestion(@RequestParam Long questionId) {
        return questionService.getById(questionId);
    }
    
    @GetMapping("/get-all")
    public List<Question> getAllQuestions() {
        return questionService.getAll();
    }
    
    @DeleteMapping
    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    public void deleteQuestion(@RequestParam Long questionId) {
        questionService.deleteById(questionId);
    }
}
