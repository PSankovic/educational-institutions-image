package com.fer.ztl.edu_institutions_image.web.controllers;

import com.fer.ztl.edu_institutions_image.appData.UniversityLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/universities")
public class UniversityController {

    @GetMapping
    public List<String> getUniversities() {
        List<String> universities = UniversityLoader.loadData();
        return universities != null ? universities : new ArrayList<>();
    }
}
