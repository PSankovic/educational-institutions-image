package com.fer.ztl.edu_institutions_image.web.dto;

import com.fer.ztl.edu_institutions_image.model.QuestionTypeEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

public class QuestionDto {
    
    private Long id;
    
    @NotEmpty
    @Size(max = 512)
    private String text;
    
    @NotEmpty
    @Positive
    private Long index;
    
    @NotNull
    private List<AnswerDto> answers;
    
    @NotNull
    private Boolean other = false;
    
    @NotEmpty
    private QuestionTypeEnum questionType;
    
    @NotNull
    private Boolean required = false;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public Long getIndex() {
        return index;
    }
    
    public void setIndex(Long index) {
        this.index = index;
    }
    
    public List<AnswerDto> getAnswers() {
        return answers;
    }
    
    public void setAnswers(List<AnswerDto> answers) {
        this.answers = answers;
    }
    
    public Boolean getOther() {
        return other;
    }
    
    public void setOther(Boolean other) {
        this.other = other;
    }
    
    public QuestionTypeEnum getQuestionType() {
        return questionType;
    }
    
    public void setQuestionType(QuestionTypeEnum questionType) {
        this.questionType = questionType;
    }
    
    public Boolean getRequired() {
        return required;
    }
    
    public void setRequired(Boolean required) {
        this.required = required;
    }
}
