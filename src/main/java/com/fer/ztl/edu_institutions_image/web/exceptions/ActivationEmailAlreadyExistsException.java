package com.fer.ztl.edu_institutions_image.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class ActivationEmailAlreadyExistsException extends RuntimeException {
}
