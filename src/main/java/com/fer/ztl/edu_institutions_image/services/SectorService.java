package com.fer.ztl.edu_institutions_image.services;

import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.model.Sector;

import java.util.List;

public interface SectorService {

// Basic
    
    Sector getById(Long id);
    
    List<Sector> getAll();
    
    Sector save(Sector sector);
    
    void delete(Sector sector);
    
    void deleteById(Long id);
    
// Specific
    
    Sector getActiveById(Long id);
    
    Sector getByName(String name);
    
    List<Sector> getActiveSectorsSorted();
    
    List<Sector> getAllSorted();
    
    void updateNameAndDescription(Long id, String name, String description);

    void deleteByName(String name);
}
