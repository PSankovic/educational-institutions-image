package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.Answer;
import com.fer.ztl.edu_institutions_image.repositories.AnswerRepository;
import com.fer.ztl.edu_institutions_image.services.AnswerService;
import com.fer.ztl.edu_institutions_image.web.exceptions.AnswerNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {
    
    private final AnswerRepository answerRepository;
    
    public AnswerServiceImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }
    
// Basic
    
    @Override
    public Answer getById(Long id) {
        return answerRepository.findById(id)
                .orElseThrow(AnswerNotFoundException::new);
    }
    
    @Override
    public List<Answer> getAll() {
        return answerRepository.findAll();
    }
    
    @Override
    public Answer save(Answer answer) {
        return answerRepository.save(answer);
    }
    
    @Override
    public void delete(Answer answer) {
        answerRepository.delete(answer);
    }
    
    @Override
    public void deleteById(Long id) {
        delete(getById(id));
    }

// Specific
    
    @Override
    public Answer increaseVoteCounter(Long id) {
        Answer answer = getById(id);
        answer.setVoteCount(answer.getVoteCount() + 1);
        return save(answer);
    }
}
