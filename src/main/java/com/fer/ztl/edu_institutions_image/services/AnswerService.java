package com.fer.ztl.edu_institutions_image.services;

import com.fer.ztl.edu_institutions_image.model.Answer;

import java.util.List;

public interface AnswerService {

// Basic
    
    Answer getById(Long id);
    
    List<Answer> getAll();
    
    Answer save(Answer answer);
    
    void delete(Answer answer);
    
    void deleteById(Long id);

// Specific

    Answer increaseVoteCounter(Long id);
}
