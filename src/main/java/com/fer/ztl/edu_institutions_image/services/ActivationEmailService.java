package com.fer.ztl.edu_institutions_image.services;


import com.fer.ztl.edu_institutions_image.model.ActivationEmail;

import java.util.List;

public interface ActivationEmailService {
    
// Basic
    
    ActivationEmail getById(Long id);
    
    List<ActivationEmail> getAll();
    
    ActivationEmail save(ActivationEmail email);
    
    void delete(ActivationEmail email);
    
    void deleteById(Long id);
    
// Specific
    
    List<ActivationEmail> getAllNonActivated();
    
    ActivationEmail getByEmail(String email);

    Boolean isPresentAndNotActivated(String email);
    
    void activateEmail(String email);
    
    void deleteEmail(String email);
}
