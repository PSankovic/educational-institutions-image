package com.fer.ztl.edu_institutions_image.services;

import com.fer.ztl.edu_institutions_image.model.Question;

import java.util.List;

public interface QuestionService {

// Basic
    
    Question getById(Long id);
    
    List<Question> getAll();
    
    Question save(Question question);
    
    void delete(Question question);
    
    void deleteById(Long id);
}
