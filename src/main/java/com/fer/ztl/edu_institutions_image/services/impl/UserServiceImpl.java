package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.User;
import com.fer.ztl.edu_institutions_image.model.util.UserDetails;
import com.fer.ztl.edu_institutions_image.repositories.UserRepository;
import com.fer.ztl.edu_institutions_image.services.UserService;
import com.fer.ztl.edu_institutions_image.web.exceptions.UserNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
// Basic
    
    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
    }
    
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
    
    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
    
    @Override
    public void delete(User user) {
        user.setDeactivated(true);
        save(user);
    }
    
    @Override
    public void deleteById(Long id) {
        delete(getById(id));
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserDetails(user);
    }
    
// Specific
    
    @Override
    public User getCurrentUser() {
        return UserDetails.getCurrentUser();
    }
    
    @Override
    public User getByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }
    
    @Override
    public User getByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }
    
    @Override
    public User getByUsernameNotDeactivated(String username) {
        User user = userRepository.findByUsernameAndDeactivatedFalse(username);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }
    
    @Override
    public void activateUser(String username) {
        User user = getByUsername(username);
        user.setDeactivated(false);
        save(user);
    }
    
    @Override
    public void deleteByUsername(String username) {
        delete(getByUsername(username));
    }
}
