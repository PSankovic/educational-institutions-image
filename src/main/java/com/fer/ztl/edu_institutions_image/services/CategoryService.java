package com.fer.ztl.edu_institutions_image.services;

import com.fer.ztl.edu_institutions_image.model.Category;
import com.fer.ztl.edu_institutions_image.model.Question;

import java.util.List;

public interface CategoryService {

// Basic
    
    Category getById(Long id);
    
    List<Category> getAll();
    
    Category save(Category category);
    
    void delete(Category category);
    
    void deleteById(Long id);

// Specific
    
    List<Question> getQuestionsForId(Long id);
}
