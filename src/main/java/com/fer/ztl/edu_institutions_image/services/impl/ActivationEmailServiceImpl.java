package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.ActivationEmail;
import com.fer.ztl.edu_institutions_image.repositories.ActivationEmailRepository;
import com.fer.ztl.edu_institutions_image.services.ActivationEmailService;
import com.fer.ztl.edu_institutions_image.web.exceptions.ActivationEmailNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivationEmailServiceImpl implements ActivationEmailService {
    
    private final ActivationEmailRepository activationEmailRepository;
    
    public ActivationEmailServiceImpl(ActivationEmailRepository activationEmailRepository) {
        this.activationEmailRepository = activationEmailRepository;
    }

// Basic
    
    @Override
    public ActivationEmail getById(Long id) {
        return activationEmailRepository.findById(id)
                .orElseThrow(ActivationEmailNotFoundException::new);
    }
    
    @Override
    public List<ActivationEmail> getAll() {
        return activationEmailRepository.findAll();
    }
    
    @Override
    public ActivationEmail save(ActivationEmail email) {
        return activationEmailRepository.save(email);
    }
    
    @Override
    public void delete(ActivationEmail email) {
        activationEmailRepository.delete(email);
    }
    
    @Override
    public void deleteById(Long id) {
        activationEmailRepository.deleteById(id);
    }

// Specific
    
    @Override
    public List<ActivationEmail> getAllNonActivated() {
        return activationEmailRepository.findAllByActivatedFalse();
    }
    
    @Override
    public ActivationEmail getByEmail(String email) {
        return activationEmailRepository.findByEmail(email);
    }
    
    @Override
    public Boolean isPresentAndNotActivated(String email) {
        return activationEmailRepository.existsByEmailAndActivatedFalse(email);
    }
    
    @Override
    public void activateEmail(String email) {
        ActivationEmail activationEmail = getByEmail(email);
        activationEmail.setActivated(true);
        save(activationEmail);
    }
    
    @Override
    public void deleteEmail(String email) {
        deleteById(getByEmail(email).getId());
    }
}
