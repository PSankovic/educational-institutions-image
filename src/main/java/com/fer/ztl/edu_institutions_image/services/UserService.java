package com.fer.ztl.edu_institutions_image.services;

import com.fer.ztl.edu_institutions_image.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

// Basic
    
    User getById(Long id);
    
    List<User> getAll();
    
    User save(User user);
    
    void delete(User user);
    
    void deleteById(Long id);
    
// Specific
    
    User getCurrentUser();
    
    User getByUsername(String username);
    
    User getByEmail(String email);
    
    User getByUsernameNotDeactivated(String username);

    void activateUser(String username);
    
    void deleteByUsername(String username);
}
