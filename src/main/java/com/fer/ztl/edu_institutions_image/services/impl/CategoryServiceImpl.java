package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.Category;
import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.repositories.CategoryRepository;
import com.fer.ztl.edu_institutions_image.services.CategoryService;
import com.fer.ztl.edu_institutions_image.services.QuestionService;
import com.fer.ztl.edu_institutions_image.web.exceptions.CategoryNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    
    private final CategoryRepository categoryRepository;
    
    private final QuestionService questionService;
    
    public CategoryServiceImpl(CategoryRepository categoryRepository, QuestionService questionService) {
        this.categoryRepository = categoryRepository;
        this.questionService = questionService;
    }

// Basic
    
    @Override
    public Category getById(Long id) {
        return categoryRepository.findById(id)
                .orElseThrow(CategoryNotFoundException::new);
    }
    
    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }
    
    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }
    
    @Override
    public void delete(Category category) {
        categoryRepository.delete(category);
        category.getQuestions().forEach(questionService::delete);
    }
    
    @Override
    public void deleteById(Long id) {
        delete(getById(id));
    }

// Specific
    
    @Override
    public List<Question> getQuestionsForId(Long id) {
        return getById(id).getQuestions();
    }
}