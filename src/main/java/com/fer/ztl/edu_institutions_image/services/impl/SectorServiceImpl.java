package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.Sector;
import com.fer.ztl.edu_institutions_image.repositories.CategoryRepository;
import com.fer.ztl.edu_institutions_image.repositories.SectorRepository;
import com.fer.ztl.edu_institutions_image.services.SectorService;
import com.fer.ztl.edu_institutions_image.web.exceptions.SectorDeletedException;
import com.fer.ztl.edu_institutions_image.web.exceptions.SectorNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectorServiceImpl implements SectorService {
    
    private final SectorRepository sectorRepository;
    
    private final CategoryRepository categoryService;
    
    public SectorServiceImpl(SectorRepository sectorRepository, CategoryRepository categoryService) {
        this.sectorRepository = sectorRepository;
        this.categoryService = categoryService;
    }
    
    @Override
    public Sector getById(Long id) {
        return sectorRepository.findById(id)
                .orElseThrow(SectorNotFoundException::new);
    }
    
    @Override
    public List<Sector> getAll() {
        return sectorRepository.findAll();
    }
    
    @Override
    public Sector save(Sector sector) {
        return sectorRepository.save(sector);
    }
    
    @Override
    public void delete(Sector sector) {
        sector.setDeleted(true);
        sector.getCategories().forEach(categoryService::delete);
        save(sector);
    }
    
    @Override
    public void deleteById(Long id) {
        delete(getById(id));
    }

// Specific
    
    @Override
    public Sector getActiveById(Long id) {
        Sector sector = getById(id);
        if (sector.getDeleted()) {
            throw new SectorDeletedException();
        }
        return sector;
    }
    
    @Override
    public Sector getByName(String name) {
        return sectorRepository.findByName(name);
    }
    
    @Override
    public List<Sector> getActiveSectorsSorted() {
        return sectorRepository.findByDeletedFalseOrderByName();
    }
    
    @Override
    public List<Sector> getAllSorted() {
        return sectorRepository.findByDeletedFalseOrderByName();
    }
    
    @Override
    public void updateNameAndDescription(Long id, String name, String description) {
        Sector sector = getById(id);
        sector.setName(name);
        sector.setDescription(description);
        save(sector);
    }
    
    @Override
    public void deleteByName(String name) {
        delete(getByName(name));
    }
}
