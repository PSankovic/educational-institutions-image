package com.fer.ztl.edu_institutions_image.services.impl;

import com.fer.ztl.edu_institutions_image.model.Question;
import com.fer.ztl.edu_institutions_image.repositories.QuestionRepository;
import com.fer.ztl.edu_institutions_image.services.AnswerService;
import com.fer.ztl.edu_institutions_image.services.QuestionService;
import com.fer.ztl.edu_institutions_image.web.exceptions.QuestionNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    
    private final QuestionRepository questionRepository;
    
    private final AnswerService answerService;
    
    public QuestionServiceImpl(QuestionRepository questionRepository, AnswerService answerService) {
        this.questionRepository = questionRepository;
        this.answerService = answerService;
    }
    
    @Override
    public Question getById(Long id) {
        return questionRepository.findById(id)
                .orElseThrow(QuestionNotFoundException::new);
    }
    
    @Override
    public List<Question> getAll() {
        return questionRepository.findAll();
    }
    
    @Override
    public Question save(Question question) {
        return questionRepository.save(question);
    }
    
    @Override
    public void delete(Question question) {
        questionRepository.delete(question);
        question.getAnswers().forEach(answerService::delete);
    }
    
    @Override
    public void deleteById(Long id) {
        delete(getById(id));
    }
}
