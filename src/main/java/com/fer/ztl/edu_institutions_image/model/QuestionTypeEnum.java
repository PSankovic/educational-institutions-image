package com.fer.ztl.edu_institutions_image.model;

public enum QuestionTypeEnum {
    CLOSE_ENDED,
    SHORT_ANSWER,
    PARAGRAPH,
    MULTIPLE_CHOICE,
    CHECKBOXES,
    DROPDOWN,
    UNIVERSITY_PICKER,
    EMOJI_SCALE,
    LINEAR_SCALE
}
