package com.fer.ztl.edu_institutions_image.model.util;

import com.fer.ztl.edu_institutions_image.model.*;
import com.fer.ztl.edu_institutions_image.repositories.SectorRepository;
import com.fer.ztl.edu_institutions_image.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DatabaseLoader implements CommandLineRunner {
    
    private final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);
    
    private final PasswordEncoder passwordEncoder;
    
    private final UserRepository userRepository;
    
    private final SectorRepository sectorRepository;
    
    public DatabaseLoader(PasswordEncoder passwordEncoder,
                          UserRepository userRepository,
                          SectorRepository sectorRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.sectorRepository = sectorRepository;
    }
    
    @Override
    public void run(String... args) {
        Set<UserRoleEnum> adminRole = new HashSet<>(List.of(UserRoleEnum.ADMIN));
        Set<UserRoleEnum> moderatorRole = new HashSet<>(List.of(UserRoleEnum.MODERATOR));
        
        try {
            User admin = new User("admin", "admin-instedurep@fer.hr",
                    passwordEncoder.encode("pass"), adminRole);
            userRepository.save(admin);
            
            Sector tourism = new Sector();
            Sector ict = new Sector();
            Sector automotiveIndustry = new Sector();
            Sector foodIndustry = new Sector();
            Sector pharmaceuticalIndustry = new Sector();
            Sector logistics = new Sector();
            Sector machineryEquipmentManufacture = new Sector();
            Sector textileIndustry = new Sector();
            Sector creativeCulturalIndustry = new Sector();
            
            tourism.setName("Turizam");
            ict.setName("ICT");
            automotiveIndustry.setName("Automobilska industrija");
            foodIndustry.setName("Prehrambena industrija");
            pharmaceuticalIndustry.setName("Farmacija");
            logistics.setName("Logistika");
            machineryEquipmentManufacture.setName("Proizvodnja opreme i strojeva");
            textileIndustry.setName("Tekstilna industrija");
            creativeCulturalIndustry.setName("Kreativna i kulturna industrija");
            
            // TODO: 12.5.2020. description
            
            tourism.setCategories(generateCategories());
            ict.setCategories(generateCategories());
            automotiveIndustry.setCategories(generateCategories());
            foodIndustry.setCategories(generateCategories());
            pharmaceuticalIndustry.setCategories(generateCategories());
            logistics.setCategories(generateCategories());
            machineryEquipmentManufacture.setCategories(generateCategories());
            textileIndustry.setCategories(generateCategories());
            creativeCulturalIndustry.setCategories(generateCategories());
            
            sectorRepository.save(tourism);
            sectorRepository.save(ict);
            sectorRepository.save(automotiveIndustry);
            sectorRepository.save(foodIndustry);
            sectorRepository.save(pharmaceuticalIndustry);
            sectorRepository.save(logistics);
            sectorRepository.save(machineryEquipmentManufacture);
            sectorRepository.save(textileIndustry);
            sectorRepository.save(creativeCulturalIndustry);
            
            Sector appFeatures = new Sector();
            appFeatures.setName("Aplikacija pokazni primjeri");
            appFeatures.setDescription("Svrha ove kategorije jest pokazati sve implementirane tipove pitanja" +
                    " i pritom isprobati funkcionalnost i potencijalnu svrhu istih.");
            appFeatures.setCategories(generateAppFeaturesCategory());
            sectorRepository.save(appFeatures);
            
        } catch (Exception ex) {
            logger.warn("Error in db-loader: " + ex.getMessage());
        }
    }
    
    private List<Category> generateCategories() {
        Category employer = new Category();
        Category student = new Category();
        
        employer.setName("Poslodavac");
        student.setName("Student");
        
        employer.setQuestions(generateEmployerQuestions());
        student.setQuestions(generateStudentQuestions());
        
        return List.of(employer, student);
    }
    
    private List<Question> generateEmployerQuestions() {
        List<Question> questions = new ArrayList<>();
        
        questions.add(multipleChoiceQuestion(
                "Molimo Vas da označite kojoj dobnoj skupini pripadate:",
                1L,
                List.of("22 - 29", "30 - 40", "41 - 50", "51 - 60", "61 i više"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Molimo Vas da procijenite koja visokoškolska ustanova u području u kojem djeluje Vaše poduzeće/Vaša " +
                        "ustanova ima najbolji imidž, te sukladno Vašem mišljenju odaberete tu ustanovu na " +
                        "popisu?",
                2L,
                true
        ));
        
        questions.add(closeEndedQuestion(
                "Uzimate li u obzir imidž institucije na kojoj su se školovali potencijalni zaposlenici prilikom " +
                        "procesa zapošljavanja?",
                3L,
                List.of("DA", "NE"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Molimo Vas da odaberete visokoškolsku ustanovu u RH koja po Vašem mišljenju ima najbolji imidž na " +
                        "tržištu rada općenito.",
                4L,
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Molimo Vas da odaberete visokoškolsku ustanovu na kojoj ste Vi studirali te potom procijenite " +
                        "slaganja ili neslaganja s niže navedenim tvrdnjama.",
                5L,
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi u mojoj zajednici imaju visoko mišljenje o fakultetu/ustanovi na kojoj sam se školovao/la.",
                6L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "U zajednici se smatra da osoba ima prestižni status ako je završila moj fakultet/stekla obrazovanje " +
                        "na ustanovi na kojoj sam se školovao/la.",
                7L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Fakultet/ustanova na kojoj sam se školovao/la se smatra jednim od najboljih sličnih/ekvivalentnih " +
                        "fakulteta.",
                8L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi s drugih sličnih/ekvivalentnih fakulteta smatraju da je fakultet/ustanova na kojoj sam se " +
                        "školovao/la manje vrijedan.",
                9L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi koji su završili sve slične/ekvivalentne fakultete bili bi ponosni kada bi njihova djeca " +
                        "pohađala fakultet/ustanovu na kojoj sam se školovao/la.",
                10L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Fakultet na kojem sam se školovao/la nije ugledan u mojoj zajednici.",
                11L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Osoba koja želi napredovati u svojoj akademskoj karijeri trebala bi što manje isticati svoju " +
                        "povezanost s fakultetom/ustanovom na kojoj se školovala.",
                12L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ako bi drugi slični/ekvivalentni fakulteti/obrazovne ustanove regrutirali nove studente, ne bi " +
                        "željeli studente s fakulteta/ustanove na kojoj sam se školovao/la.",
                13L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Imidž fakulteta na kojem sam studirao/la značajno je utjecao na moju odluku vezano za odabir tog fakulteta.",
                14L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(closeEndedQuestion(
                "U slučaju da se mogu vratiti i ponovo birati, odabrao/la bi isti fakultet na kojem sam studirao.",
                15L,
                List.of("DA", "NE"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "U slučaju da je odgovor na prethodno pitanje NE, molimo Vas da u padajućem izborniku odaberete " +
                        "fakultet za koji bi se odlučili kada bi ponovo birali:",
                16L,
                false
        ));
        
        questions.add(emojiScaleQuestion(
                "Na kraju, molimo Vas da odaberete osjećajnik (emoji) koji najprikladnije opisuje Vaše " +
                        "studiranje na visokoškolskoj ustanovi koju ste pohađali.",
                17L,
                getEmojiScaleAnswers(),
                true
        ));
        
        return questions;
    }
    
    private List<Question> generateStudentQuestions() {
        List<Question> questions = new ArrayList<>();
        
        questions.add(multipleChoiceQuestion(
                "Molimo Vas da označite kojoj dobnoj skupini pripadate:",
                1L,
                List.of("18 - 21", "22 - 25", "26 - 29", "30 - 35", "35 i više"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Molimo Vas da odaberete visokoškolsku ustanovu u RH koja po Vašem mišljenju ima najbolji imidž.",
                2L,
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Molimo Vas da odaberete visokoškolsku ustanovu na kojoj studirate te potom procijenite slaganja" +
                        " ili neslaganja s niže navedenim tvrdnjama.",
                3L,
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi u mojoj zajednici imaju visoko mišljenje o fakultetu/ustanovi na kojoj studiram.",
                4L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "U zajednici se smatra da osoba ima prestižni status ako je završila moj fakultet/stekla obrazovanje " +
                        "na ustanovi na kojoj sam se školujem.",
                5L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Fakultet/ustanova na kojoj sam se školujem smatra se jednim od najboljih sličnih/ekvivalentnih " +
                        "fakulteta.",
                6L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi s drugih sličnih/ekvivalentnih fakulteta smatraju da je fakultet/ustanova na kojoj se " +
                        "školujem manje vrijedna",
                7L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ljudi koji su završili sve slične/ekvivalentne fakultete bili bi ponosni kada bi njihova djeca " +
                        "pohađala fakultet/ustanovu na kojoj se školujem.",
                8L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Fakultet na kojem se školujem nije ugledan u mojoj zajednici.",
                9L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Osoba koja želi napredovati u svojoj akademskoj karijeri trebala bi što manje isticati svoju " +
                        "povezanost s fakultetom/ustanovom na kojoj se školuje.",
                10L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Ako bi drugi slični/ekvivalentni fakulteti/obrazovne ustanove regrutirali nove studente, ne bi " +
                        "željeli studente s fakulteta/ustanove na kojoj se školujem.",
                11L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Imidž fakulteta na kojem studiram značajno je utjecao na moju odluku vezano za odabir tog fakulteta.",
                12L,
                getLinearScaleAnswers(),
                true
        ));
        
        questions.add(closeEndedQuestion(
                "U slučaju da se mogu vratiti i ponovo birati, odabrao/la bi isti fakultet na kojem studiram.",
                13L,
                List.of("DA", "ne"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "U slučaju da je odgovor na prethodno pitanje NE, molimo Vas da u padajućem izborniku odaberete " +
                        "fakultet za koji bi se odlučili u slučaju da birate ponovo:",
                14L,
                false
        ));
        
        questions.add(emojiScaleQuestion(
                "Na kraju, molimo Vas da odaberete osjećajnik (emoji) koji najprikladnije opisuje Vaše " +
                        "studiranje na visokoškolskoj ustanovi koju pohađate.",
                15L,
                getEmojiScaleAnswers(),
                true
        ));
        
        return questions;
    }
    
    private List<Category> generateAppFeaturesCategory() {
        Category employer = new Category();
        Category student = new Category();
        
        employer.setName("Poslodavac");
        student.setName("Student");
        
        employer.setQuestions(generateAppFeaturesQuestions());
        student.setQuestions(generateAppFeaturesQuestions());
        
        return List.of(employer, student);
    }
    
    private List<Question> generateAppFeaturesQuestions() {
        List<Question> questions = new ArrayList<>();
        
        questions.add(closeEndedQuestion(
                "Primjer pitanja zatvorenog odgovora.",
                1L,
                List.of("DA", "NE"),
                true
        ));
        
        questions.add(shortAnswerQuestion(
                "Primjer pitanja kratkog odgovora (max. 64 znakova).",
                2L,
                true
        ));
        
        questions.add(paragraphQuestion(
                "Primjer pitanja dugog odgovora - paragraph (max. 256 znakova).",
                3L,
                true
        ));
        
        questions.add(multipleChoiceQuestion(
                "Primjer pitanja višestrukog izbora (moguće odabrati samo 1 odgovor).",
                4L,
                List.of("1. odgovor", "2. odgovor", "3. odgovor", "4. odgovor"),
                true
        ));
        
        questions.add(checkboxQuestion(
                "Primjer pitanja s odabirnim okvirima - checkboxes (moguće odabrati više odgovora).",
                5L,
                List.of("1. odgovor", "2. odgovor", "3. odgovor", "4. odgovor"),
                true
        ));
        
        questions.add(dropdownQuestion(
                "Primjer pitanja s padajućim izbornikom.",
                6L,
                List.of("1. odgovor", "2. odgovor", "3. odgovor", "4. odgovor"),
                true
        ));
        
        questions.add(universityPickerQuestion(
                "Primjer pitanja koje u padajućem izborniku ima ponuđen popis svih ustanova iz sustava " +
                        "visokog obrazovanja u Republici Hrvatskoj.",
                7L,
                true
        ));
        
        questions.add(emojiScaleQuestion(
                "Primjer pitanja s odabirom simbola za osjećaje (emotikoni, osjećajnici).",
                8L,
                getEmojiScaleAnswers(),
                true
        ));
        
        questions.add(linearScaleQuestion(
                "Primjer pitanja s linearnom ljestvicom povezanih odgovora.",
                9L,
                getLinearScaleAnswers(),
                true
        ));
        
        return questions;
    }
    
    private Question closeEndedQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.CLOSE_ENDED, required);
    }
    
    private Question shortAnswerQuestion(String text, Long index, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        return new Question(text, index, answers, QuestionTypeEnum.SHORT_ANSWER, required);
    }
    
    private Question paragraphQuestion(String text, Long index, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        return new Question(text, index, answers, QuestionTypeEnum.PARAGRAPH, required);
    }
    
    private Question multipleChoiceQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.MULTIPLE_CHOICE, required);
    }
    
    private Question checkboxQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.CHECKBOXES, required);
    }
    
    private Question dropdownQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.DROPDOWN, required);
    }
    
    private Question universityPickerQuestion(String text, Long index, Boolean required) {
        return new Question(text, index, new ArrayList<>(), QuestionTypeEnum.UNIVERSITY_PICKER, required);
    }
    
    private List<String> getEmojiScaleAnswers() {
        return List.of(
                "Vrlo nezadovoljan/na",
                "Donekle nezadovoljan/na",
                "Ni zadovoljan/na ni nezadovoljan/na",
                "Donekle zadovoljan/na",
                "Vrlo zadovoljan/na"
        );
    }
    
    private Question emojiScaleQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.EMOJI_SCALE, required);
    }
    
    private List<String> getLinearScaleAnswers() {
        return List.of(
                "U potpunosti se ne slažem",
                "Uglavnom se ne slažem",
                "Niti se slažem, niti se ne slažem",
                "Uglavnom se slažem",
                "U potpunosti se slažem"
        );
    }
    
    private Question linearScaleQuestion(String text, Long index, List<String> answerList, Boolean required) {
        List<Answer> answers = new ArrayList<>();
        for (int i = 0; i < answerList.size(); i++) {
            answers.add(new Answer(answerList.get(i), i + 1L));
        }
        return new Question(text, index, answers, QuestionTypeEnum.LINEAR_SCALE, required);
    }
}
