package com.fer.ztl.edu_institutions_image.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Question {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotEmpty
    @Size(max = 512)
    private String text;
    
    @Positive
    private Long index;
    
    @OneToMany(cascade = CascadeType.ALL)
    @NotNull
    private List<Answer> answers;
    
    @Enumerated(EnumType.STRING)
    private QuestionTypeEnum questionType;
    
    @NotNull
    private Boolean required = false;
    
    public Question() {
    }
    
    public Question(@NotEmpty @Size(max = 512) String text, @NotEmpty @Positive Long index,
                    @NotNull List<Answer> answers, QuestionTypeEnum questionType, @NotNull Boolean required) {
        this.text = text;
        this.index = index;
        this.answers = answers;
        this.questionType = questionType;
        this.required = required;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public Long getIndex() {
        return index;
    }
    
    public void setIndex(Long index) {
        this.index = index;
    }
    
    public List<Answer> getAnswers() {
        return answers;
    }
    
    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
    
    public void addAnswer(Answer answer) {
        if (answers == null) {
            answers = new ArrayList<>();
        }
        answers.add(answer);
    }
    
    public QuestionTypeEnum getQuestionType() {
        return questionType;
    }
    
    public void setQuestionType(QuestionTypeEnum questionType) {
        this.questionType = questionType;
    }
    
    public Boolean getRequired() {
        return required;
    }
    
    public void setRequired(Boolean required) {
        this.required = required;
    }
}
