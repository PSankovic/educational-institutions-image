package com.fer.ztl.edu_institutions_image.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "APP_USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(unique = true)
    @NotEmpty
    @Size(min = 2, max = 32)
    private String username;
    
    @Column(unique = true)
    @Email
    @NotEmpty
    @Size(max = 256)
    private String email;
    
    @NotEmpty
    private String passwordHash;
    
    @ElementCollection(targetClass = UserRoleEnum.class, fetch = FetchType.EAGER)
    @JoinTable(name = "USER_ROLES", joinColumns = @JoinColumn(name = "USER_ID"))
    @Column(name = "ROLE", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotEmpty
    private Set<UserRoleEnum> roles = new HashSet<>();
    
    @NotNull
    private Boolean deactivated = false;
    
    public User() {
    }
    
    public User(String username, String email, String passwordHash, Set<UserRoleEnum> roles) {
        this.username = username;
        this.email = email;
        this.passwordHash = passwordHash;
        this.roles = new HashSet<>(roles);
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPasswordHash() {
        return passwordHash;
    }
    
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    
    public Set<UserRoleEnum> getRoles() {
        return roles;
    }
    
    public void addRole(UserRoleEnum roleEnum) {
        this.roles.add(roleEnum);
    }
    
    public void setRoles(Set<UserRoleEnum> roles) {
        this.roles = roles;
    }
    
    public Boolean getDeactivated() {
        return deactivated;
    }
    
    public void setDeactivated(Boolean deactivated) {
        this.deactivated = deactivated;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
