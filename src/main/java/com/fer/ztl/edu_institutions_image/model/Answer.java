package com.fer.ztl.edu_institutions_image.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotEmpty
    @Size(max = 256)
    private String text;
    
    @Positive
    private Long index;
    
    @NotNull
    private Long voteCount = 0L;
    
    public Answer() {
    }
    
    public Answer(String text, Long index) {
        this.text = text;
        this.index = index;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public Long getIndex() {
        return index;
    }
    
    public void setIndex(Long index) {
        this.index = index;
    }
    
    public Long getVoteCount() {
        return voteCount;
    }
    
    public void setVoteCount(Long voteCount) {
        this.voteCount = voteCount;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Answer answer = (Answer) o;
        return Objects.equals(id, answer.id);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
