package com.fer.ztl.edu_institutions_image.model;

public enum UserRoleEnum {
    ADMIN,
    MODERATOR
}
