package com.fer.ztl.edu_institutions_image.repositories;

import com.fer.ztl.edu_institutions_image.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
