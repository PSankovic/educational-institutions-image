package com.fer.ztl.edu_institutions_image.repositories;

import com.fer.ztl.edu_institutions_image.model.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectorRepository extends JpaRepository<Sector, Long> {
    
    Sector findByName(String name);
    
    List<Sector> findByDeletedFalseOrderByName();
}
