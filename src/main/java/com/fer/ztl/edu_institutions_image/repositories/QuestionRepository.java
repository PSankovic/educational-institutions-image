package com.fer.ztl.edu_institutions_image.repositories;

import com.fer.ztl.edu_institutions_image.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}
