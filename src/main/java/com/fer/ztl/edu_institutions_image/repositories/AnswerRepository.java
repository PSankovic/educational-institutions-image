package com.fer.ztl.edu_institutions_image.repositories;

import com.fer.ztl.edu_institutions_image.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
