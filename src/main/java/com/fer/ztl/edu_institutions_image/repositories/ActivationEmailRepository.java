package com.fer.ztl.edu_institutions_image.repositories;

import com.fer.ztl.edu_institutions_image.model.ActivationEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivationEmailRepository extends JpaRepository<ActivationEmail, Long> {
    
    List<ActivationEmail> findAllByActivatedFalse();
    
    ActivationEmail findByEmail(String email);
    
    Boolean existsByEmailAndActivatedFalse(String email);
}
