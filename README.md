# Bachelor final project

Programsko rješenje za istraživanja utjecaja imidža ubrazovnih unstitucija u procesu zapošljavanja

Software solution for researching the impact of image of educational institutions in the employment process

### Deployed to Microsoft Azure:
- link to [front end page](https://instedurep.azurewebsites.net)
- link to [back end page](https://instedurep-back-end.herokuapp.com)

### Documentation:
- [Bachelor final paper](https://gitlab.com/PSankovic/educational-institutions-image/-/blob/master/ZR_Paulo_Sankovi%C4%87.pdf)

### Used Technologies and Tools:

| Technology and Tool | Version |
| ------ | ------ |
| [Git](https://git-scm.com) | 2.22.0.windows.1|
| [GitLab](https://gitlab.com/) | |
| [IntelliJ IDEA](https://www.jetbrains.com/idea/) | 2020.1.2 |
| [Spring Boot](https://spring.io/projects/spring-boot) | 2.2.5 |
| [Java](https://www.java.com/en/) | 11.0.2 |
| [React](https://reactjs.org/) | 16.13.0 |
| [Material-UI](https://material-ui.com/) | core - 4.9.14<br>icons - 4.9.1<br>lab - 4.0.0-alpha.53 |
| [CanvasJs](https://canvasjs.com/) | 2.3.2 |
| [Microsoft Azure](https://azure.microsoft.com/en-us/) | |
| [Heroku](https://www.heroku.com/) | |
| [PostgreSQL](https://www.postgresql.org/) | 12.1 |
| [Postman](https://www.postman.com/) | 7.25.0 |
| [Mozilla Firefox](https://www.mozilla.org/hr/firefox/new/) | 77.0 |
| [Google Chrome](https://www.google.com/intl/hr_HR/chrome/) | 83.0.4103.97 |
| [Microsoft Edge](https://www.microsoft.com/hr-hr/edge) | 44.17763.831.0 |